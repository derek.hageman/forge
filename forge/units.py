import typing


ZERO_C_IN_K = 273.15
ONE_ATM_IN_HPA = 1013.25


def flow_ccm_to_lpm(ccm: float) -> float:
    return ccm / 1000.0


def flow_ccs_to_lpm(ccs: float) -> float:
    return flow_ccm_to_lpm(ccs * 60.0)


def flow_m3s_to_lpm(m3s: float) -> float:
    return m3s * (1000 * 60.0)


def flow_lpm_to_ccm(lpm: float) -> float:
    return lpm * 1000.0


def flow_lpm_to_ccs(lpm: float) -> float:
    return flow_lpm_to_ccm(lpm) / 60.0


def flow_lpm_to_m3s(lpm: float) -> float:
    return lpm / (60.0 * 1000.0)


def temperature_k_to_c(k: float) -> float:
    return k - ZERO_C_IN_K


def temperature_f_to_c(f: float) -> float:
    return (f - 32.0) * (5.0 / 9.0)


def pressure_Pa_to_hPa(p: float) -> float:
    return p / 100.0


def pressure_kPa_to_hPa(p: float) -> float:
    return p * 10.0


def pressure_bar_to_hPa(p: float) -> float:
    return p * 1000.0


def pressure_mmHg_to_hPa(p: float) -> float:
    return p * 1.33322387415


def pressure_inHg_to_hPa(p: float) -> float:
    return p * 33.86388158


def mass_ng_to_ug(m: float) -> float:
    return m / 1000.0


def concentration_ppm_to_ppb(x: float) -> float:
    return x * 1000.0


def distance_m_to_km(x: float) -> float:
    return x / 1000.0


def distance_km_to_m(x: float) -> float:
    return x * 1000.0


def distance_in_to_mm(x: float) -> float:
    return x * 25.4


def speed_knots_to_ms(x: float) -> float:
    return x * 0.5144444444


def speed_mph_to_ms(x: float) -> float:
    return x * 0.44704


def speed_kph_to_ms(x: float) -> float:
    return distance_km_to_m(x) / (60.0 * 60.0)


def speed_fpm_to_ms(x: float) -> float:
    return x * 0.00508
