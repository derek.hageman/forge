import typing
import numpy as np
import forge.data.structure.variable as netcdf_var
import forge.data.structure.timeseries as netcdf_timeseries
from math import nan
from .converter import InstrumentConverter


class Converter(InstrumentConverter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._average_interval: typing.Optional[float] = None

    @property
    def tags(self) -> typing.Optional[typing.Set[str]]:
        return {"aerosol", "cpc", "tsi3010cpc"}

    @property
    def instrument_type(self) -> typing.Optional[str]:
        return "tsi3010cpc"

    @property
    def average_interval(self) -> typing.Optional[float]:
        return self._average_interval

    @property
    def split_monitor(self) -> typing.Optional[bool]:
        return None

    def run(self) -> bool:
        data_N = self.load_variable(f"N_{self.instrument_id}")
        if data_N.time.shape[0] == 0:
            return False
        self._average_interval = self.calculate_average_interval(data_N.time)
        if not super().run():
            return False

        data_T1 = self.load_variable(f"T1_{self.instrument_id}")
        data_T2 = self.load_variable(f"T2_{self.instrument_id}")

        g, times = self.data_group([data_N])
        self.declare_system_flags(g, times)

        var_N = g.createVariable("number_concentration", "f8", ("time",), fill_value=nan)
        netcdf_var.variable_number_concentration(var_N)
        netcdf_timeseries.variable_coordinates(g, var_N)
        var_N.variable_id = "N"
        var_N.coverage_content_type = "physicalMeasurement"
        var_N.cell_methods = "time: mean"
        self.apply_data(times, var_N, data_N)

        split_monitor = self.split_monitor
        if split_monitor is None:
            split_monitor = self.calculate_split_monitor(data_T1.time)
        if not split_monitor:
            mon_g = g
            mon_times = times
        elif data_T1.time.shape[0] > 0:
            mon_g, mon_times = self.data_group([data_T1], name='status', fill_gaps=False)
        else:
            mon_g, mon_times = None, None
            split_monitor = True

        if mon_g is not None:
            var_T1 = mon_g.createVariable("saturator_temperature", "f8", ("time",), fill_value=nan)
            netcdf_var.variable_temperature(var_T1)
            netcdf_timeseries.variable_coordinates(mon_g, var_T1)
            var_T1.variable_id = "T1"
            var_T1.coverage_content_type = "physicalMeasurement"
            var_T1.cell_methods = "time: mean"
            var_T1.long_name = "saturator block temperature"
            self.apply_data(mon_times, var_T1, data_T1)

            var_T2 = mon_g.createVariable("condenser_temperature", "f8", ("time",), fill_value=nan)
            netcdf_var.variable_temperature(var_T2)
            netcdf_timeseries.variable_coordinates(mon_g, var_T2)
            var_T2.variable_id = "T2"
            var_T2.coverage_content_type = "physicalMeasurement"
            var_T2.cell_methods = "time: mean"
            var_T2.long_name = "condenser temperature"
            self.apply_data(mon_times, var_T2, data_T2)

        if not split_monitor:
            self.apply_cut_size(g, times, [
                (var_N, data_N),
                (var_T1, data_T1),
                (var_T2, data_T2),
            ])
        else:
            self.apply_cut_size(g, times, [
                (var_N, data_N),
            ])
        self.apply_coverage(g, times, f"N_{self.instrument_id}")

        self.apply_instrument_metadata(f"N_{self.instrument_id}", manufacturer="TSI", model="3010")

        return True