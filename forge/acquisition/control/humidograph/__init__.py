
def main():
    from .control import Control
    from ..run import launch

    launch(Control)
