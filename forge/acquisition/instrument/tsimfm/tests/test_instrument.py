import asyncio
import typing
import pytest
from forge.tasks import wait_cancelable
from forge.acquisition.instrument.testing import create_streaming_instrument, cleanup_streaming_instrument, BusInterface
from forge.acquisition.instrument.tsimfm.simulator import Simulator
from forge.acquisition.instrument.tsimfm.instrument import Instrument



@pytest.mark.asyncio
async def test_communications():
    simulator: Simulator = None
    instrument: Instrument = None
    simulator, instrument = await create_streaming_instrument(Instrument, Simulator)
    bus: BusInterface = instrument.context.bus

    simulator_run = asyncio.ensure_future(simulator.run())
    instrument_run = asyncio.ensure_future(instrument.run())

    await wait_cancelable(bus.wait_for_communicating(), 30)

    assert await bus.value('Q') == pytest.approx(simulator.data_Q, abs=0.1)
    assert await bus.value('T') == simulator.data_T
    assert await bus.value('P') == simulator.data_P
    assert await bus.value('U') == simulator.data_U

    await cleanup_streaming_instrument(simulator, instrument, instrument_run, simulator_run)


@pytest.mark.asyncio
async def test_4000_series():
    simulator: Simulator = None
    instrument: Instrument = None
    simulator, instrument = await create_streaming_instrument(Instrument, Simulator)
    bus: BusInterface = instrument.context.bus

    simulator.model_number = b"4040"
    simulator.data_U = None

    simulator_run = asyncio.ensure_future(simulator.run())
    instrument_run = asyncio.ensure_future(instrument.run())

    await wait_cancelable(bus.wait_for_communicating(), 30)

    assert await bus.value('Q') == pytest.approx(simulator.data_Q, abs=0.1)
    assert await bus.value('T') == simulator.data_T
    assert await bus.value('P') == simulator.data_P

    await cleanup_streaming_instrument(simulator, instrument, instrument_run, simulator_run)


