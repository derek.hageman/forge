import typing
import asyncio
import logging
import re
import forge.data.structure.variable as netcdf_var
from math import isfinite
from forge.acquisition import LayeredConfiguration
from .standard import StandardInstrument
from .variable import Input, Variable, VariableVectorMagnitude, VariableVectorDirection
from .state import Persistent, State
from ..cutsize import CutSize

_LOGGER = logging.getLogger(__name__)


def _merge_attribute(config: LayeredConfiguration, attributes: typing.Dict[str, str]) -> None:
    attrs = config.get('ATTRIBUTES')
    if attrs:
        for attr in attrs.keys():
            attributes[attr] = str(config.get(attr))

    long_name = config.get("LONG_NAME", default=config.get('DESCRIPTION'))
    if long_name:
        attributes['long_name'] = long_name
    C_format = config.get("C_FORMAT", default=config.get('FORMAT'))
    if C_format:
        attributes['C_format'] = C_format
    units = config.get('UNITS')
    if units:
        attributes['units'] = units


class AnalogInput:
    def __init__(self, name: str, config: LayeredConfiguration, inp: Input):
        self.name = name
        self.config = config
        self.input = inp
        self.variable: typing.Optional[Variable] = None
        self.attributes: typing.Dict[str, str] = dict()

    def __call__(self, value: float) -> None:
        self.input(value)

    @property
    def value(self) -> typing.Optional[float]:
        return self.input.value
        
    @classmethod
    def construct(cls, instrument: StandardInstrument, name: str,
                  config: LayeredConfiguration) -> typing.Optional["AnalogInput"]:
        raise NotImplementedError

    @classmethod
    def create_inputs(cls, instrument: StandardInstrument):
        variable_config = instrument.context.config.get('DATA')
        if not isinstance(variable_config, dict):
            return list()

        pending_wind_speed: typing.Dict[str, typing.Tuple[Input, str, str, typing.Dict[str, typing.Any]]] = dict()
        pending_wind_direction: typing.Dict[str, typing.Tuple[Input, str, str, typing.Dict[str, typing.Any]]] = dict()

        def wind_speed(source: Input, name: str = None, code: str = None,
                       attributes: typing.Dict[str, typing.Any] = None) -> None:
            if code and code.startswith("WS"):
                associated = code[2:]
            elif name and "wind_speed" in name:
                associated = name.replace("wind_speed", "")
            else:
                associated = ""

            if associated in pending_wind_speed:
                _LOGGER.warning("Duplicate wind speed association, one will be ignored")

            pending_wind_speed[associated] = (source, name, code, attributes)

        def wind_direction(source: Input, name: str = None, code: str = None,
                           attributes: typing.Dict[str, typing.Any] = None) -> None:
            if code and code.startswith("WD"):
                associated = code[2:]
            elif name and "wind_direction" in name:
                associated = name.replace("wind_direction", "")
            else:
                associated = ""

            if associated in pending_wind_direction:
                _LOGGER.warning("Duplicate wind direction association, one will be ignored")

            pending_wind_direction[associated] = (source, name, code, attributes)

        type_map: typing.Dict[str, typing.Callable] = {
            'temperature': instrument.variable_temperature,
            't': instrument.variable_temperature,
            'air_temperature': instrument.variable_air_temperature,
            'air_t': instrument.variable_air_temperature,
            'rh': instrument.variable_rh,
            'u': instrument.variable_rh,
            'air_rh': instrument.variable_air_rh,
            'air_u': instrument.variable_air_rh,
            'pressure': instrument.variable_pressure,
            'p': instrument.variable_pressure,
            'air_pressure': instrument.variable_air_pressure,
            'air_p': instrument.variable_air_pressure,
            'flow': instrument.variable_flow,
            'q': instrument.variable_flow,
            'sample_flow': instrument.variable_sample_flow,
            'sample_q': instrument.variable_sample_flow,
            'delta_pressure': instrument.variable_delta_pressure,
            'dp': instrument.variable_delta_pressure,
            'wind_speed': wind_speed,
            'ws': wind_speed,
            'wind_direction': wind_direction,
            'wd': wind_direction,
            'none': instrument.variable,
        }
        name_match: typing.Dict["re.Pattern", typing.Callable] = {
            re.compile(r'^T\d*(?:_|$)'): instrument.variable_temperature,
            re.compile(r'^U\d*(?:_|$)'): instrument.variable_rh,
            re.compile(r'^Pd\d*(?:_|$)'): instrument.variable_delta_pressure,
            re.compile(r'^P\d*(?:_|$)'): instrument.variable_pressure,
            re.compile(r'^Q\d*(?:_|$)'): instrument.variable_flow,
            re.compile(r'^WS\d*(?:_|$)'): wind_speed,
            re.compile(r'^WD\d*(?:_|$)'): wind_direction,
        }

        analog_inputs: typing.List["AnalogInput"] = list()
        names = list(variable_config.keys())
        names.sort()
        for name in names:
            ain = cls.construct(instrument, name, instrument.context.config.section('DATA', name))
            if ain is None:
                continue
            analog_inputs.append(ain)
            if ain.config.get('DISABLE_LOGGING'):
                continue

            _merge_attribute(ain.config, ain.attributes)

            default_variable = instrument.variable
            for pattern, value in name_match.items():
                if not pattern.search(name):
                    continue
                default_variable = value
                break
            type_name = ain.config.get('TYPE')
            if isinstance(type_name, str):
                type_name = type_name.lower()
            create = type_map.get(type_name, default_variable)
            ain.variable = create(ain.input,
                                  name=ain.config.get('FIELD'),
                                  code=name,
                                  attributes=ain.attributes)

        for associated, direction in pending_wind_direction.items():
            (direction_source, name_direction, code_direction, attributes_direction) = direction
            speed = pending_wind_speed.pop(associated, None)
            if not speed:
                _LOGGER.warning(f"No wind speed available for {name_direction}, data will not be logged")
                continue
            (speed_source, name_speed, code_speed, attributes_speed) = speed

            var_speed, var_direction = instrument.variable_winds(
                speed_source, direction_source,
                name_speed=name_speed, name_direction=name_direction,
                code_speed=code_speed, code_direction=code_direction,
                attributes_speed=attributes_speed, attributes_direction=attributes_direction
            )

            for ain in analog_inputs:
                if ain.input == direction_source:
                    ain.variable = var_direction
                elif ain.input == speed_source:
                    ain.variable = var_speed

        for associated, speed in pending_wind_speed.items():
            _LOGGER.warning(f"No wind direction available for {speed[1]}, data will not be logged")

        return analog_inputs


class AnalogOutput:
    REMEMBER_CHANGES = False

    def __init__(self, name: str, config: LayeredConfiguration):
        self.name = name
        self.config = config
        self.persistent: Persistent = None
        self.state: typing.Optional[State] = None
        self.attributes: typing.Dict[str, str] = dict()

        self.command_channel: typing.Optional[typing.Any] = None

    def __call__(self, value: float) -> None:
        self.persistent(value)

    @property
    def value(self) -> typing.Optional[float]:
        return self.persistent.value

    @value.setter
    def value(self, v: float) -> None:
        self(v)

    def command_received(self) -> None:
        pass

    def _command_set_analog_channel(self, data: typing.Dict[str, typing.Any]) -> None:
        if not self.command_channel:
            return
        if not isinstance(data, dict):
            return
        channel = data.get('channel')
        value = data.get('value')
        try:
            channel = type(self.command_channel)(channel)
            value = float(value)
        except (ValueError, TypeError, OverflowError):
            return
        if channel != self.command_channel:
            return
        if not isfinite(value):
            return
        self(value)
        self.command_received()

    def _command_set_analog(self, data: typing.Dict[str, typing.Any]) -> None:
        if not isinstance(data, dict):
            return
        output = data.get('output')
        value = data.get('value')
        try:
            output = str(output)
            value = float(value)
        except (ValueError, TypeError, OverflowError):
            return
        if output != self.name:
            return
        if not isfinite(value):
            return
        self(value)
        self.command_received()

    @classmethod
    def construct(cls, instrument: StandardInstrument, name: str,
                  config: LayeredConfiguration) -> typing.Optional["AnalogOutput"]:
        raise NotImplementedError

    @classmethod
    def create_outputs(cls, instrument: StandardInstrument, section: str = 'ANALOG_OUTPUT'):
        output_config = instrument.context.config.get(section)
        if not isinstance(output_config, dict):
            return list()

        analog_outputs: typing.List["AnalogOutput"] = list()
        names = list(output_config.keys())
        names.sort()
        for name in names:
            aot = cls.construct(instrument, name, instrument.context.config.section_or_constant(section, name))
            if aot is None:
                continue
            analog_outputs.append(aot)

            instrument.context.bus.connect_command('set_analog_channel', aot._command_set_analog_channel)
            instrument.context.bus.connect_command('set_analog', aot._command_set_analog)

            if not isinstance(aot.config, LayeredConfiguration):
                aot.persistent = instrument.persistent(name, send_to_bus=False, save_value=False)
                continue

            save_value = bool(aot.config.get('REMEMBER_CHANGES', default=cls.REMEMBER_CHANGES))

            if not aot.config.get('ENABLE_LOGGING'):
                aot.persistent = instrument.persistent(name, send_to_bus=False, save_value=save_value)
            else:
                _merge_attribute(aot.config, aot.attributes)

                aot.persistent = instrument.persistent(name, send_to_bus=True, save_value=save_value)
                aot.state = instrument.state_float(aot.persistent,
                                                   name=aot.config.get('FIELD'),
                                                   code=name,
                                                   attributes=aot.attributes)

            if not save_value or aot.persistent.value is None:
                initial = aot.config.get('INITIAL')
                if initial is not None:
                    aot.persistent(float(initial), oneshot=True)

            shutdown = aot.config.get('SHUTDOWN')
            if shutdown is not None:
                aot.shutdown_state = float(shutdown)

        return analog_outputs


class DigitalOutput:
    REMEMBER_CHANGES = False

    def __init__(self, name: str, config: LayeredConfiguration):
        self.name = name
        self.config = config
        self.persistent: Persistent = None

        self.command_bit: typing.Optional[int] = None

        self.cut_size_state: typing.Dict[CutSize.Size, bool] = dict()
        self.bypass_state: typing.Dict[bool, bool] = dict()
        self.shutdown_state: typing.Optional[bool] = None
        
        self._prior_cut_size: typing.Optional[CutSize.Size] = None
        self._prior_bypass: typing.Optional[bool] = None

    def __call__(self, value: bool) -> None:
        self.persistent(value)

    @property
    def value(self) -> typing.Optional[bool]:
        return self.persistent.value

    @value.setter
    def value(self, v: bool) -> None:
        self(v)

    def _configure_cut_size(self, config: typing.Union[str, dict]) -> None:
        if not isinstance(config, dict):
            set_state = CutSize.Size.parse(config)
            for clear in CutSize.Size:
                self.cut_size_state[clear] = False
            self.cut_size_state[set_state] = True
            return

        for cut_size, state in config.items():
            cut_size = CutSize.Size.parse(cut_size)
            state = bool(state)
            self.cut_size_state[cut_size] = state

    def _configure_bypass(self, config: typing.Union[bool, dict]) -> None:
        if not isinstance(config, dict):
            config = bool(config)
            self.bypass_state[config] = True
            self.bypass_state[not config] = False
            return

        for bypass, state in config.items():
            if isinstance(bypass, str):
                bypass = bypass.lower()
                if bypass == 'on' or bypass == 'true' or bypass == 't' or bypass == 'bypass':
                    bypass = True
                elif bypass == 'off' or bypass == 'false' or bypass == 'false' or bypass == 'sample':
                    bypass = False
            bypass = bool(bypass)
            state = bool(state)
            self.bypass_state[bypass] = state

    def command_received(self) -> None:
        pass

    def _command_set_digital_output(self, data: int) -> None:
        if not self.command_bit:
            return
        try:
            bits = int(data)
        except (ValueError, TypeError, OverflowError):
            return
        is_set = (bits & self.command_bit) != 0
        self(is_set)
        self.command_received()

    def _command_set_digital(self, data: typing.Dict[str, typing.Any]) -> None:
        if not isinstance(data, dict):
            return
        output = data.get('output')
        value = data.get('value')
        try:
            output = str(output)
            value = bool(value)
        except (ValueError, TypeError, OverflowError):
            return
        if output != self.name:
            return
        self(value)
        self.command_received()

    @classmethod
    def construct(cls, instrument: StandardInstrument, name: str,
                  config: LayeredConfiguration) -> typing.Optional["DigitalOutput"]:
        raise NotImplementedError

    @classmethod
    def create_outputs(cls, instrument: StandardInstrument, section: str = 'DIGITAL'):
        output_config = instrument.context.config.get(section)
        if not isinstance(output_config, dict):
            return list()

        digital_outputs: typing.List["DigitalOutput"] = list()
        names = list(output_config.keys())
        names.sort()
        for name in names:
            dot = cls.construct(instrument, name, instrument.context.config.section_or_constant(section, name))
            if dot is None:
                continue
            digital_outputs.append(dot)

            instrument.context.bus.connect_command('set_digital_output', dot._command_set_digital_output)
            instrument.context.bus.connect_command('set_digital', dot._command_set_digital)

            if not isinstance(dot.config, LayeredConfiguration):
                dot.persistent = instrument.persistent(name, send_to_bus=False, save_value=False)
                continue

            save_value = bool(dot.config.get('REMEMBER_CHANGES', default=cls.REMEMBER_CHANGES))

            dot.persistent = instrument.persistent(name, send_to_bus=False, save_value=save_value)
            if not save_value:
                initial = dot.config.get('INITIAL')
                if initial is not None:
                    dot.persistent(bool(initial), oneshot=True)

            shutdown = dot.config.get('SHUTDOWN')
            if shutdown is not None:
                dot.shutdown_state = bool(shutdown)

            cut_size = dot.config.get('CUT_SIZE')
            if cut_size is not None:
                dot._configure_cut_size(cut_size)

            bypass = dot.config.get('BYPASS')
            if bypass is not None:
                dot._configure_bypass(bypass)
        return digital_outputs

    def update_cut_size(self, cut_size: typing.Optional[CutSize.Size]) -> None:
        if cut_size is None:
            self._prior_cut_size = None
            return
        if cut_size == self._prior_cut_size:
            return
        self._prior_cut_size = cut_size
        target = self.cut_size_state.get(cut_size)
        if target is None:
            return
        self(target)

    def update_bypass(self, bypassed: bool) -> None:
        if bypassed == self._prior_bypass:
            return
        self._prior_bypass = bypassed
        target = self.bypass_state.get(bypassed)
        if target is None:
            return
        self(target)
