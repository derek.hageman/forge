import typing
import asyncio
import time
import re
from math import nan
from forge.tasks import wait_cancelable
from forge.units import flow_ccm_to_lpm, pressure_kPa_to_hPa
from ..streaming import StreamingInstrument, StreamingContext, CommunicationsError
from ..parse import parse_number, parse_flags_bits

_INSTRUMENT_TYPE = __name__.split('.')[-2]
_RV_RESPONSE = re.compile(rb"Model\s+(?:3789|(?:375.))\s+Ver(?:sion)?\s+(\S+)\s+(?:(?:S/N)|(?:Ser(?:ial)?\s*Num(?:ber)?))\s+\d+")


class Instrument(StreamingInstrument):
    INSTRUMENT_TYPE = _INSTRUMENT_TYPE
    MANUFACTURER = "TSI"
    MODEL = "375x"
    DISPLAY_LETTER = "C"
    TAGS = frozenset({"aerosol", "cpc", _INSTRUMENT_TYPE})
    SERIAL_PORT = {'baudrate': 115200}

    def __init__(self, context: StreamingContext):
        super().__init__(context)

        self._report_interval: int = int(context.config.get('REPORT_INTERVAL', default=1))
        self._sleep_time: float = 0.0
        self._have_sample_flow: bool = True
        self._have_optics_extra: bool = True
        self._have_photometric_voltage: bool = False

        self.data_N = self.input("N")
        self.data_C = self.input("C")
        self.data_P = self.input("P")
        self.data_PDnozzle = self.input("PDnozzle")
        self.data_PDorifice = self.input("PDorifice")
        self.data_Alaser = self.input("Alaser")
        self.data_liquid_level = self.input("liquid_level")
        self.data_Vphoto = self.input("Vphoto")

        self.data_Q = self.input("Q")
        self.data_Qinlet = self.input("Qinlet")
        self.data_Qinstrument = self.input("Qinstrument")

        self.data_Tsaturator = self.input("Tsaturator")
        self.data_Tcondenser = self.input("Tcondenser")
        self.data_Toptics = self.input("Toptics")
        self.data_Tcabinet = self.input("Tcabinet")

        if not self.data_N.field.comment and self.data_Q.field.comment:
            self.data_N.field.comment = self.data_Q.field.comment
        if not self.data_N.field.comment and self.data_Qinstrument.field.comment:
            self.data_N.field.comment = self.data_Qinstrument.field.comment

        self.notify_liquid_low = self.notification('liquid_low', is_warning=True)
        self.bit_flags: typing.Dict[int, Instrument.Notification] = {
            0x0040: self.notify_liquid_low,
        }

        self.variable_saturator = self.variable_temperature(
            self.data_Tsaturator, "saturator_temperature", code="T1",
            attributes={'long_name': "saturator block temperature"}
        )
        self.variable_condenser = self.variable_temperature(
            self.data_Tcondenser, "condenser_temperature", code="T2",
            attributes={'long_name': "condenser temperature"}
        )

        self.instrument_report = self.report(
            self.variable_number_concentration(self.data_N, code="N"),

            self.variable_sample_flow(self.data_Q, code="Q",
                                      attributes={'C_format': "%5.3f"}),

            self.variable_air_pressure(self.data_P, "pressure", code="P",
                                       attributes={'long_name': "absolute pressure"}),

            self.variable_delta_pressure(self.data_PDorifice, "orifice_pressure_drop", code="Pd2", attributes={
                'long_name': "orifice pressure drop",
                'C_format': "%4.0f",
            }),

            self.variable_saturator,
            self.variable_condenser,
            self.variable_temperature(self.data_Tcabinet, "cabinet_temperature", code="T4",
                                      attributes={'long_name': "internal cabinet temperature"}),

            flags=[
                self.flag_bit(self.bit_flags, 0x0001, "saturator_temperature_out_of_range", is_warning=True),
                self.flag_bit(self.bit_flags, 0x0002, "condenser_temperature_out_of_range", is_warning=True),
                self.flag_bit(self.bit_flags, 0x0004, "optics_temperature_error"),
                self.flag_bit(self.bit_flags, 0x0008, "inlet_flow_error", is_warning=True),
                self.flag_bit(self.bit_flags, 0x0010, "sample_flow_error", is_warning=True),
                self.flag_bit(self.bit_flags, 0x0020, "laser_power_error", is_warning=True),
                self.flag(self.notify_liquid_low, preferred_bit=0x0040),
                self.flag_bit(self.bit_flags, 0x0080, "concentration_out_of_range"),
            ],
        )

        self.report_not_3757: typing.Optional[StreamingInstrument.Report] = None
        self.report_not_3750_3752: typing.Optional[StreamingInstrument.Report] = None

    def _update_model_report(self, model: str) -> None:
        if model != "3757" and not self.report_not_3757:
            self.report_not_3757 = self.report(
                self.variable_delta_pressure(self.data_PDnozzle, "nozzle_pressure_drop", code="Pd1", attributes={
                    'long_name': "nozzle pressure drop",
                    'C_format': "%6.2f",
                }),

                self.variable_temperature(self.data_Toptics, "optics_temperature", code="T3",
                                          attributes={'long_name': "optics block temperature"}),

                self.variable(self.data_Alaser, "laser_current", code="A", attributes={
                    'long_name': "laser current",
                    'units': "mA",
                    'C_format': "%3.0f"
                }),

                automatic=False,
            )

        if model != "3750" and model != "3752" and not self.report_not_3750_3752:
            self.report_not_3750_3752 = self.report(
                self.variable_flow(self.data_Qinlet, "inlet_flow", code="Qu", attributes={
                    'long_name': "inlet flow rate",
                    'C_format': "%5.3f",
                }),

                automatic=False,
            )

        if model == "3789":
            self.variable_saturator.data.name = "initiator_temperature"
            self.variable_saturator.data.attributes["long_name"] = "initiator temperature"

            self.variable_condenser.data.name = "conditioner_temperature"
            self.variable_condenser.data.attributes["long_name"] = "conditioner temperature"
        else:
            self.variable_saturator.data.name = "saturator_temperature"
            self.variable_saturator.data.attributes["long_name"] = "saturator temperature"

            self.variable_condenser.data.name = "condenser_temperature"
            self.variable_condenser.data.attributes["long_name"] = "condenser temperature"

    async def start_communications(self) -> None:
        if not self.writer:
            raise CommunicationsError

        self.writer.write(b"\r" * 8)
        await self.writer.drain()
        await self.drain_reader(0.5)

        self.writer.write(b"RMN\r")
        try:
            data: bytes = await wait_cancelable(self.read_line(), 2.0)
            if data == b"ERROR":
                raise TimeoutError
        except (TimeoutError, asyncio.TimeoutError):
            self.writer.write(b"RMN\r")
            data: bytes = await wait_cancelable(self.read_line(), 2.0)
        if data == b"ERROR":
            raise CommunicationsError
        try:
            model = data.decode('utf-8')
        except UnicodeError as e:
            raise CommunicationsError from e
        model = model.strip()
        if not model.startswith("375") and model != "3789":
            raise CommunicationsError(f"unsupported model {model}")
        self.set_instrument_info('model', model)
        self._update_model_report(model)
        self._have_sample_flow = (model != "3750" and model != "3752")
        self._have_optics_extra = (model != "3757")
        self._have_photometric_voltage = (model == "3752")

        self.writer.write(b"RSN\r")
        data: bytes = await wait_cancelable(self.read_line(), 2.0)
        if data == b"ERROR":
            raise CommunicationsError
        self.set_serial_number(data)

        self.writer.write(b"RV\r")
        data: bytes = await wait_cancelable(self.read_line(), 2.0)
        matched = _RV_RESPONSE.fullmatch(data)
        if not matched:
            raise CommunicationsError(f"invalid version {data}")
        self.set_firmware_version(matched.group(1))

        self.writer.write(b"RIE\r")
        data: bytes = await wait_cancelable(self.read_line(), 2.0)
        try:
            flags = int(data, 16)
            if flags < 0 or flags > 0xFFFF:
                raise ValueError
        except ValueError:
            raise CommunicationsError(f"invalid flags {data}")

        # Not sure what ACTRIS wants for these, so turn on the correction
        self.writer.write(b"SCC,1\r")
        data: bytes = await wait_cancelable(self.read_line(), 2.0)
        if data != b"OK":
            raise CommunicationsError(f"invalid response {data}")

        self._sleep_time = 0.0
        await self.communicate()
        self._sleep_time = 0.0

    async def communicate(self) -> None:
        if not self.writer:
            raise CommunicationsError
        if self._sleep_time > 0.0:
            await asyncio.sleep(self._sleep_time)
            self._sleep_time = 0.0
        begin_read = time.monotonic()

        self.writer.write(b"RALL\r")
        line: bytes = await wait_cancelable(self.read_line(), 2.0)
        if len(line) < 3:
            raise CommunicationsError

        fields = line.split(b',')
        try:
            (
                N, flags, Tsaturator, Tcondenser, Toptics, Tcabinet,
                P, PDorifice, PDnozzle, Alaser, liquid_level
            ) = fields
        except ValueError:
            raise CommunicationsError(f"invalid number of fields in {line}")

        if self._have_sample_flow:
            self.writer.write(b"RSF\r")
            Qsample = await wait_cancelable(self.read_line(), 2.0)
        else:
            Qsample = None

        self.writer.write(b"RIF\r")
        Qinlet = await wait_cancelable(self.read_line(), 2.0)

        if self._have_photometric_voltage:
            self.writer.write(b"R7\r")
            Vphoto = await wait_cancelable(self.read_line(), 2.0)
            self.data_Vphoto(parse_number(Vphoto))
        else:
            self.data_Vphoto(nan)

        self.data_Tsaturator(parse_number(Tsaturator))
        self.data_Tcondenser(parse_number(Tcondenser))
        self.data_Tcabinet(parse_number(Tcabinet))
        self.data_P(pressure_kPa_to_hPa(parse_number(P)))
        self.data_PDorifice(pressure_kPa_to_hPa(parse_number(PDorifice)))

        if self._have_optics_extra:
            self.data_Toptics(parse_number(Toptics))
            self.data_PDnozzle(pressure_kPa_to_hPa(parse_number(PDnozzle)))
            self.data_Alaser(parse_number(Alaser))
        else:
            self.data_Toptics(nan)
            self.data_PDnozzle(nan)
            self.data_Alaser(nan)

        if Qsample is not None:
            self.data_Qinlet(parse_number(Qinlet))
            Qinstrument = self.data_Qinstrument(flow_ccm_to_lpm(parse_number(Qsample)))
        else:
            self.data_Qinlet(nan)
            Qinstrument = self.data_Qinstrument(flow_ccm_to_lpm(parse_number(Qinlet)))
        Q = self.data_Q(Qinstrument)

        N = parse_number(N)
        N *= Qinstrument / Q
        self.data_N(N)

        parse_flags_bits(flags, self.bit_flags)
        if liquid_level.startswith(b"FULL ("):
            self.notify_liquid_low(False)

            adc_level = liquid_level[6:]
            try:
                adc_level = adc_level[:adc_level.index(b')')]
            except ValueError:
                pass
            try:
                adc_level = int(adc_level)
                if adc_level < 0 or adc_level > 0xFFFF:
                    raise ValueError
                self.data_liquid_level(adc_level)
            except ValueError:
                raise CommunicationsError(f"invalid liquid level {liquid_level}")
        elif liquid_level.startswith(b"NOTFULL ("):
            self.notify_liquid_low(True)

            adc_level = liquid_level[6:]
            try:
                adc_level = adc_level[:adc_level.index(b')')]
            except ValueError:
                pass
            try:
                adc_level = int(adc_level)
                if adc_level < 0 or adc_level > 0xFFFF:
                    raise ValueError
                self.data_liquid_level(adc_level)
            except ValueError:
                raise CommunicationsError(f"invalid liquid level {liquid_level}")
        elif liquid_level == b"FULL":
            self.notify_liquid_low(False)
        elif liquid_level == b"NOTFULL":
            self.notify_liquid_low(True)
        else:
            try:
                adc_level = int(liquid_level)
                if adc_level < 0 or adc_level > 0xFFFF:
                    raise ValueError
                self.data_liquid_level(adc_level)
            except ValueError:
                raise CommunicationsError(f"invalid liquid level {liquid_level}")

        if self.report_not_3757:
            self.report_not_3757()
        if self.report_not_3750_3752:
            self.report_not_3750_3752()
        self.instrument_report()
        end_read = time.monotonic()
        self._sleep_time = self._report_interval - (end_read - begin_read)
