import typing
import asyncio
import signal
import argparse
import logging
import importlib.util
from importlib import import_module
from pathlib import Path
from forge.acquisition import LayeredConfiguration, CONFIGURATION
from forge.acquisition.average import AverageRecord
from .base import BaseInstrument, BaseDataOutput, BaseBusInterface, BasePersistentInterface, BaseContext


_LOGGER = logging.getLogger(__name__)


def arguments() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Forge acquisition instrument.")

    parser.add_argument('--debug',
                        dest='debug', action='store_true',
                        help="enable debug output")
    parser.add_argument('--systemd',
                        dest='systemd', action='store_true',
                        help="enable systemd integration")

    parser.add_argument('--data-working',
                        dest='data_working',
                        help="directory to place in progress data files")
    parser.add_argument('--data-completed',
                        dest='data_completed',
                        help="directory to place completed data files")
    parser.add_argument('--state-location',
                        dest='state_location',
                        help="directory to place state files in")

    parser.add_argument('type',
                        help="instrument type code")
    parser.add_argument('identifier',
                        help="instrument identifier code")

    return parser


def main():
    args, _ = arguments().parse_known_args()
    if args.debug:
        from forge.log import set_debug_logger
        set_debug_logger()

    if '/' in args.type:
        spec = importlib.util.spec_from_file_location("forge.acquisition.instrument.external", args.type)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        mod.main()
    else:
        import_module('.', 'forge.acquisition.instrument.' + args.type).main()


def instrument_config(args: argparse.Namespace) -> LayeredConfiguration:
    roots: typing.List[dict] = list()

    instrument_local = CONFIGURATION.get("INSTRUMENT." + args.identifier)
    toml = None
    if instrument_local is not None:
        roots.append(instrument_local)
        toml = LayeredConfiguration.configuration_toml("INSTRUMENT." + args.identifier)

    global_config = CONFIGURATION.get("ACQUISITION.GLOBAL")
    if global_config is not None:
        roots.append(global_config)
        if toml is None:
            toml = LayeredConfiguration.configuration_toml("ACQUISITION.GLOBAL")

    return LayeredConfiguration(*roots, toml=toml)


def _global_section_override(args: argparse.Namespace, section: str) -> LayeredConfiguration:
    roots: typing.List[dict] = list()

    instrument_local = CONFIGURATION.get("INSTRUMENT." + args.identifier + "." + section)
    toml = None
    if instrument_local is not None:
        roots.append(instrument_local)
        toml = LayeredConfiguration.configuration_toml("INSTRUMENT." + args.identifier + "." + section)

    global_config = CONFIGURATION.get("ACQUISITION." + section)
    if global_config is not None:
        roots.append(global_config)
        if toml is None:
            toml = LayeredConfiguration.configuration_toml("ACQUISITION." + section)

    return LayeredConfiguration(*roots, toml=toml)


def average_config(args: argparse.Namespace) -> LayeredConfiguration:
    return _global_section_override(args, "AVERAGE")


def cutsize_config(args: argparse.Namespace) -> LayeredConfiguration:
    return _global_section_override(args, "CUT_SIZE")


def bus_interface(args: argparse.Namespace) -> BaseBusInterface:
    from .businterface import BusInterface

    interface = BusInterface(args.identifier.upper(), CONFIGURATION.get("ACQUISITION.BUS", '/run/forge-acquisition-bus.socket'))

    bypass_config = _global_section_override(args, "BYPASS")
    enabled = bypass_config.constant()
    if enabled is not None:
        if not enabled:
            interface.bypass_only_source = set()
        elif isinstance(enabled, list):
            interface.bypass_only_source = set()
            for v in enabled:
                interface.bypass_only_source.add(v)
    else:
        for v in bypass_config.get("IGNORE_FROM", default=[]):
            interface.bypass_ignore_source.add(v)
        for v in bypass_config.get("ONLY_FROM", default=[]):
            if interface.bypass_only_source is None:
                interface.bypass_only_source = set()
            interface.bypass_only_source.add(v)

    return interface


def data_directories(args: argparse.Namespace) -> typing.Tuple[typing.Optional[Path], typing.Optional[Path]]:
    working_directory = args.data_working or CONFIGURATION.get("ACQUISITION.DATA_TEMP")
    if working_directory:
        working_directory = Path(working_directory)
    else:
        working_directory = None

    completed_directory = args.data_completed or CONFIGURATION.get("ACQUISITION.DATA_COMPLETED")
    if completed_directory:
        completed_directory = Path(completed_directory)
    else:
        completed_directory = None

    return working_directory, completed_directory


def data_output(args: argparse.Namespace) -> BaseDataOutput:
    from .dataoutput import DataOutput

    roots: typing.List[dict] = list()

    instrument_local = CONFIGURATION.get("INSTRUMENT." + args.identifier + ".FILE")
    toml = None
    if instrument_local is not None:
        roots.append(instrument_local)
        toml = LayeredConfiguration.configuration_toml("INSTRUMENT." + args.identifier + ".FILE")

    global_config = CONFIGURATION.get("ACQUISITION.FILE")
    if global_config is not None:
        roots.append(global_config)
        if toml is None:
            toml = LayeredConfiguration.configuration_toml("ACQUISITION.FILE")

    working_directory, completed_directory = data_directories(args)

    return DataOutput(CONFIGURATION.get("ACQUISITION.STATION", 'nil').lower(), args.identifier.upper(),
                      LayeredConfiguration(*roots, toml=toml),
                      working_directory, completed_directory,
                      AverageRecord(average_config(args)).interval)


def persistent_interface(args: argparse.Namespace) -> BasePersistentInterface:
    from .persistent import PersistentInterface

    state_file = f"{args.identifier.upper()}-{args.type.lower()}.json"
    state_location = args.state_location or CONFIGURATION.get("ACQUISITION.STATE_LOCATION", "/var/lib/forge/state")
    state_location = Path(state_location)
    if not state_location.is_dir():
        raise NotADirectoryError(f"invalid state location: {state_location}")

    return PersistentInterface(state_location / state_file)


def prepare_context(instrument: BaseInstrument) -> None:
    from .dataoutput import DataOutput
    data: DataOutput = instrument.context.data

    data.instrument_type = instrument.INSTRUMENT_TYPE
    data.tags.update(instrument.TAGS)

    def add_tags(tags):
        if isinstance(tags, str):
            tags = tags.split()
        if isinstance(tags, bool):
            return
        for t in tags:
            t = t.strip()
            if not t:
                continue
            data.tags.add(t)

    tags = instrument.context.config.get("TAGS")
    if tags:
        add_tags(tags)
    tags = instrument.context.config.get("ALL_TAGS")
    if tags:
        data.tags.clear()
        add_tags(tags)


def run(instrument: BaseInstrument, systemd: bool = False) -> None:
    loop = asyncio.get_event_loop()
    prepare_context(instrument)

    _LOGGER.debug("Starting bus interface")
    loop.run_until_complete(instrument.context.bus.start())

    _LOGGER.debug("Starting data interface")
    loop.run_until_complete(instrument.context.data.start())

    heartbeat: typing.Optional[asyncio.Task] = None
    if systemd:
        import systemd.daemon
        systemd.daemon.notify("READY=1")

        _LOGGER.debug("Starting systemd heartbeat")

        async def send_heartbeat() -> typing.NoReturn:
            while True:
                await asyncio.sleep(10)
                systemd.daemon.notify("WATCHDOG=1")

        heartbeat = loop.create_task(send_heartbeat())

    _LOGGER.debug("Instrument start")
    instrument_run = loop.create_task(instrument.run())
    loop.add_signal_handler(signal.SIGINT, instrument_run.cancel)
    loop.add_signal_handler(signal.SIGTERM, instrument_run.cancel)
    try:
        loop.run_until_complete(instrument_run)
    except asyncio.CancelledError:
        pass
    _LOGGER.debug("Instrument shutdown")

    if heartbeat:
        _LOGGER.debug("Shutting down heartbeat")
        t = heartbeat
        heartbeat = None
        try:
            t.cancel()
        except:
            pass
        try:
            loop.run_until_complete(t)
        except:
            pass

    _LOGGER.debug("Shutting down data interface")
    loop.run_until_complete(instrument.context.data.shutdown())

    _LOGGER.debug("Shutting down bus interface")
    loop.run_until_complete(instrument.context.bus.shutdown())

    _LOGGER.debug("Shutdown complete")


def launch(create_instrument: typing.Callable[[BaseContext], BaseInstrument]) -> None:
    args = arguments()
    args = args.parse_args()
    bus = bus_interface(args)
    data = data_output(args)
    persistent = persistent_interface(args)
    config = instrument_config(args)

    ctx = BaseContext(config, data, bus, persistent)
    ctx.average_config = average_config(args)
    ctx.cutsize_config = cutsize_config(args)

    instrument = create_instrument(ctx)
    ctx.persistent.version = instrument.PERSISTENT_VERSION
    run(instrument, args.systemd)
