import typing


def license(station: str, tags: typing.Optional[typing.Set[str]] = None) -> typing.Optional[str]:
    return "https://creativecommons.org/publicdomain/zero/1.0/"
