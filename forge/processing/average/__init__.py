STANDARD_QUANTILES = [
    0.0,
    0.00135,
    0.00621,
    0.02275,
    0.05,
    0.06681,
    0.15866,
    0.25,
    0.30854,
    0.50,
    0.69146,
    0.75,
    0.84134,
    0.93319,
    0.95,
    0.97725,
    0.99379,
    0.99865,
    1.0
]
