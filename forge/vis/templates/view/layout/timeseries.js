//{% if realtime %}
TimeSelect.setIntervalBounds();
//{% endif %}

let layout = {
    //{% if view.title %}
    title: "{{ view.title }}",
    //{% endif %}

    autosize : true,
    hovermode: 'x',

    xaxis: TimeSeriesCommon.getXAxis(),

    grid: {
        columns: 1,
        rows: '{{ view.graphs|length }}' * 1,
        pattern: 'coupled',
        ygap: 0.3,
    },

    annotations: [
        //{% for graph in view.graphs %}{% if graph.display_title %}
        {
            xanchor: 'center',
            yanchor: 'bottom',
            xref: 'paper',
            yref: '{{ view.axis_code(graph.axes[0]) }} domain',
            y: 1,
            x: 0.5,
            text: "{{ graph.title }}",
            showarrow: false,
            captureevents: true,
        },
        //{% endif %}{% endfor %}
    ],

    //{% set axis_loop = namespace(have_right=False) %}
    //{% for graph in view.graphs %}
    //  {% for axis in graph.axes %}
    "{{ view.axis_code(axis, base='yaxis') }}": {
        side: '{% if axis.side %}{{ axis.side }}{% else %}{% if loop.index%2 == 1 %}left{% else %}right{% set axis_loop.have_right = True %}{% endif %}{% endif %}',
        //{% if loop.index > 1 %}
        overlaying: '{{ view.axis_code(graph.axes[0]) }}',
        zeroline: false,
        showgrid: false,
        //{% endif %}

        //{% if axis.title %}
        title: "{{ axis.title }}",
        //{% endif %}

        //{% if axis.hover_format %}
        hoverformat: "{{ axis.hover_format }}",
        //{% endif %}

        type: '{% if axis.logarithmic %}log{% else %}linear{% endif %}',
        //{% if axis.range == 0 %}
        rangemode: 'tozero',
        //{% elif axis.range %}
        range: ['{{ axis.range[0] }}' * 1, '{{ axis.range[1] }}' * 1],
        //{% endif %}

        //{% if axis.ticks %}
        tickvals: [
            // {% for tick in axis.ticks %}
            '{{ tick }}' * 1,
            // {% endfor %}
        ],
        //{% endif %}
    },
    //  {% endfor %}
    //{% endfor %}

    legend: {
        groupclick: 'toggleitem',
        tracegroupgap: 30,
        traceorder: 'grouped',
        //{% if axis_loop.have_right %}
        x: 1.06,
        //{% endif %}
    },
};

let data = [
    //{% set trace_loop = namespace(index=0) %}
    //{% for graph in view.graphs %}
    //{% set graph_index = loop.index0 %}
    //  {% for trace in graph.traces %}
    {
        x: [ ],
        y: [ ],
        mode: 'lines',
        yaxis: '{{ view.axis_code(trace.axis) }}',
        legendgroup: '{{ graph_index }}',
        name: "{{ trace.legend }}",
        hovertemplate: "{{ trace.hover_template() }}",
        line: {
            width: 1,
            //{% if trace.color %}
            color: '{{ trace.color }}',
            //{% endif %}
        },
        marker: {
            symbol: '{% if trace_loop.index <= 52 %}{{ trace_loop.index }}{% elif trace_loop.index <= 52*2 %}{{ trace_loop.index + 200 }}{% endif %}',
        },
    },
    //  {% set trace_loop.index = trace_loop.index + 1 %}
    //  {% endfor %}
    //{% endfor %}
];

let config = {
    responsive: true,
};

const div = document.getElementById('view_timeseries');
if (localStorage.getItem('forge-settings-plot-scroll')) {
    div.classList.add('scroll');
}

Plotly.newPlot(div, data, layout, config);

const replotController = new ReplotController(div, data, layout, config);
const shapeHandler = new ShapeHandler(replotController);

// {% if not realtime %}
const traces = new TimeSeriesCommon.Traces(replotController);
// {% else %}
const traces = new TimeSeriesCommon.RealtimeTraces(replotController);
// {% endif %}
