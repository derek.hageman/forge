var TimeSeriesCommon = {};
(function() {
    TimeSeriesCommon.getXAxis = function() {
        const result = {
            title: "UTC",
            type: 'date',
            hoverformat: '%Y-%m-%d %H:%M:%S',
            tickformat: '%H:%M\n %Y-%m-%d',
            zeroline: false,
            autorange: false,
            range: [DataSocket.toPlotTime(TimeSelect.start_ms), DataSocket.toPlotTime(TimeSelect.end_ms)],
        };

        if (localStorage.getItem('forge-settings-time-format') === 'doy') {
            result.hoverformat = "%Y:%-j.%k";
            result.tickformat = "%-j.%k\n %Y";
        }

        return result;
    }

    const emptyShapes = [];

    TimeSeriesCommon.updateShapes = function() { }

    const selectedTimeHighlights = [];
    TimeSeriesCommon.getTimeHighlights = function() { return selectedTimeHighlights; }
    TimeSelect.onHighlight( (start_ms, end_ms) => {
        selectedTimeHighlights.length = 0;
        if (start_ms === undefined && end_ms === undefined) {
            TimeSeriesCommon.updateShapes();
            return;
        }
        if (!start_ms) {
            start_ms = TimeSelect.start_ms;
        }
        if (!end_ms) {
            end_ms = TimeSelect.end_ms;
        }

        if (start_ms === end_ms) {
            selectedTimeHighlights.push({
                type: 'line',
                xref: 'x',
                yref: 'paper',
                x0: DataSocket.toPlotTime(start_ms),
                y0: 0,
                x1: DataSocket.toPlotTime(end_ms),
                y1: 1,
                opacity: 0.6,
                line: {
                    width: 2,
                    color: '#44aaff',
                }
            });
        } else {
            selectedTimeHighlights.push({
                type: 'rect',
                xref: 'x',
                yref: 'paper',
                x0: DataSocket.toPlotTime(start_ms),
                y0: 0,
                x1: DataSocket.toPlotTime(end_ms),
                y1: 1,
                fillcolor: '#44aaff',
                opacity: 0.4,
                line: {
                    width: 2,
                    color: '#44aaff',
                }
            });
        }

        TimeSeriesCommon.updateShapes();
    });

    const contaminationShapes = new Map();
    TimeSeriesCommon.clearContamination = function() {
        contaminationShapes.clear();
    }
    TimeSeriesCommon.installContamination = function(shapeHandler, record, yref) {
        const key = {};
        DataSocket.addLoadedRecord(record, (dataName) => { return new Contamination.DataStream(dataName); },
            (start_ms, end_ms) => {
                let target = contaminationShapes.get(key);
                if (target === undefined) {
                    target = [];
                    contaminationShapes.set(key, target);
                }

                const x0 = DataSocket.toPlotTime(start_ms);
                const x1 = DataSocket.toPlotTime(end_ms);
                if (target.length > 0 && target[target.length-1].x1 === x0) {
                    target[target.length-1].x1 = x1;
                } else {
                    target.push({
                        type: 'line',
                        layer: 'below',
                        xref: 'x',
                        yref: yref + ' domain',
                        x0: DataSocket.toPlotTime(start_ms),
                        x1: DataSocket.toPlotTime(end_ms),
                        y0: 0.05,
                        y1: 0.05,
                        opacity: 0.9,
                        line: {
                            width: 5,
                            color: '#000000',
                        },
                    });
                }

                shapeHandler.update();
            }, () => {
                shapeHandler.update(true);
            });

        shapeHandler.generators.push(() => {
            const shapes = contaminationShapes.get(key);
            if (shapes === undefined) {
                return emptyShapes;
            }
            return shapes;
        });
    }

    let hideContaminatedData = false;

    const AVERAGE_NONE = 0;
    const AVERAGE_HOUR = 1;
    const AVERAGE_DAY = 2;
    const AVERAGE_MONTH = 3;
    let averagingMode = sessionStorage.getItem('forge-plot-average-mode') || AVERAGE_NONE;
    averagingMode = averagingMode * 1;
    if (averagingMode !== AVERAGE_NONE) {
        hideContaminatedData = true;
    }
    if (sessionStorage.getItem('forge-plot-hide-contamination') !== null) {
        hideContaminatedData = sessionStorage.getItem('forge-plot-hide-contamination') === "1";
    }
    TimeSeriesCommon.overrideAveraging = function(mode) {
        if (mode === 'hour') {
            averagingMode = AVERAGE_HOUR;
            hideContaminatedData = true;
        } else if (mode === 'day') {
            averagingMode = AVERAGE_DAY;
            hideContaminatedData = true;
        } else if (mode === 'month') {
            averagingMode = AVERAGE_MONTH;
            hideContaminatedData = true;
        } else {
            averagingMode = AVERAGE_NONE;
            hideContaminatedData = false;
        }
    }

    function addControlButton(options) {
        const controlBar = document.getElementById('control_bar');
        const controlButton = document.createElement('button');
        controlButton.classList.add('control-button');

        if (options) {
            const hoverRoot = document.createElement('div');
            hoverRoot.classList.add('control-dropdown')
            controlBar.appendChild(hoverRoot);

            controlButton.classList.add('control-dropdown-button');
            hoverRoot.appendChild(controlButton);

            const menu = document.createElement('ul');
            hoverRoot.appendChild(menu);
            menu.classList.add('control-dropdown-content');

            for (const o of options) {
                const item = document.createElement('li');
                menu.appendChild(item);

                const optionButton = document.createElement('button');
                optionButton.classList.add('text-content');
                item.appendChild(optionButton);

                optionButton.textContent = o.text;
                $(optionButton).click(o.click);
                o.button = optionButton;
            }
        } else {
            controlBar.appendChild(controlButton);
        }

        return controlButton;
    }

    TimeSeriesCommon.addContaminationToggleButton = function(traces) {
        function apply() {
            if (hideContaminatedData) {
                button.classList.remove('mdi-filter');
                button.classList.add('mdi-filter-off');
                button.text = 'Control display of contaminated data (currently hidden)';
                options[0].button.classList.remove('active');
                options[1].button.classList.add('active');
            } else {
                button.classList.remove('mdi-filter-off');
                button.classList.add('mdi-filter');
                button.text = 'Control display of contaminated data (currently shown)';
                options[1].button.classList.remove('active');
                options[0].button.classList.add('active');
            }
        }

        function update(event, hide) {
            event.preventDefault();
            hideContaminatedData = hide;
            sessionStorage.setItem('forge-plot-hide-contamination', hideContaminatedData && "1" || "0");
            apply();
            traces.updateDisplay(true);
        }

        const options = [
            {
                text: "Show contamination",
                click: function(event) { update(event, false); },
            },
            {
                text: "Hide contamination",
                click: function(event) { update(event, true); },
            },
        ];
        const button = addControlButton(options);
        button.classList.add('mdi', 'contamination-toggle', 'hidden');
        button.title = 'Control display of contaminated data';
        options[0].button.classList.add("show-contamination");
        options[1].button.classList.add("hide-contamination");

        apply();
    }

    let showSymbols = false;
    if (sessionStorage.getItem('forge-plot-show-symbols') !== null) {
        showSymbols = sessionStorage.getItem('forge-plot-show-symbols') === "1";
    }
    TimeSeriesCommon.addSymbolToggleButton = function(traces) {
        function apply() {
            if (showSymbols) {
                button.classList.remove('mdi-chart-line-variant');
                button.classList.add('mdi-chart-bubble');
                button.title = 'Control display of symbols a data points (symbols shown)';
                traces.data.forEach((trace) => {
                    if (trace.mode === 'lines') {
                        trace.mode = 'lines+markers';
                    }
                });
                options[0].button.classList.remove('active');
                options[1].button.classList.add('active');
            } else {
                button.classList.remove('mdi-chart-bubble');
                button.classList.add('mdi-chart-line-variant');
                button.title = 'Control display of symbols a data points (symbols hidden)';
                traces.data.forEach((trace) => {
                    if (trace.mode === 'lines+markers') {
                        trace.mode = 'lines';
                    }
                });
                options[1].button.classList.remove('active');
                options[0].button.classList.add('active');
            }
        }

        function update(event, show) {
            event.preventDefault();
            showSymbols = show;
            sessionStorage.setItem('forge-plot-show-symbols', showSymbols && "1" || "0");
            apply();
            traces.updateDisplay(true);
        }

        const options = [
            {
                text: "Hide data point symbols",
                click: function(event) { update(event, false); },
            },
            {
                text: "Show data point symbols",
                click: function(event) { update(event, true); },
            },
        ];
        const button = addControlButton(options);
        button.classList.add('mdi', 'mdi-chart-line-variant');
        button.title = 'Control display of symbols a data points';

        apply();
    }

    TimeSeriesCommon.addAveragingButton = function(traces) {
        function apply() {
            switch (averagingMode) {
            default:
                button.classList.add('mdi', 'mdi-sine-wave');
                button.classList.remove('character-icon');
                button.textContent = "";
                button.title = 'Set plot averaging (current: no averaging)';
                break;
            case AVERAGE_HOUR:
                button.classList.remove('mdi', 'mdi-sine-wave');
                button.classList.add('character-icon');
                button.textContent = "H";
                button.title = 'Set plot averaging (current: one hour average)';
                break;
            case AVERAGE_DAY:
                button.classList.remove('mdi', 'mdi-sine-wave');
                button.classList.add('character-icon');
                button.textContent = "D";
                button.title = 'Set plot averaging (current: one day average)';
                break;
            case AVERAGE_MONTH:
                button.classList.remove('mdi', 'mdi-sine-wave');
                button.classList.add('character-icon');
                button.textContent = "M";
                button.title = 'Set plot averaging (current: monthly average)';
                break;
            }
            for (let i=0; i<options.length; i++) {
                if (averagingMode === i) {
                    options[i].button.classList.add('active');
                } else {
                    options[i].button.classList.remove('active');
                }
            }
        }

        function update(event, averaging) {
            event.preventDefault();
            averagingMode = averaging;
            sessionStorage.setItem('forge-plot-average-mode', averagingMode.toString());

            if (averaging === AVERAGE_NONE) {
                if (hideContaminatedData && sessionStorage.getItem('forge-plot-hide-contamination') === null) {
                    $('button.show-contamination').click();
                    sessionStorage.removeItem('forge-plot-hide-contamination');
                }
            } else {
                if (!hideContaminatedData && sessionStorage.getItem('forge-plot-hide-contamination') === null) {
                    $('button.hide-contamination').click();
                    sessionStorage.removeItem('forge-plot-hide-contamination');
                }
            }

            apply();
            traces.updateDisplay(true);
        }

        const options = [
            {
                text: "No averaging",
                click: function(event) { update(event, AVERAGE_NONE); },
            },
            {
                text: "One hour average",
                click: function(event) { update(event, AVERAGE_HOUR); },
            },
            {
                text: "One day average",
                click: function(event) { update(event, AVERAGE_DAY); },
            },
            {
                text: "One month average",
                click: function(event) { update(event, AVERAGE_MONTH); },
            },
        ];
        const button = addControlButton(options);
        button.classList.add('mdi', 'mdi-sine-wave');
        button.title = 'Set plot averaging';

        apply();
    }

    const HOVER_SEPARATE = 0;
    const HOVER_SINGLE_POINT = 1;
    const HOVER_COMBINED = 2;
    const HOVER_OFF = 3;
    let hoverMode = sessionStorage.getItem('forge-plot-hover-mode') || HOVER_SEPARATE;
    hoverMode = hoverMode * 1;
    TimeSeriesCommon.addHoverControlButton = function(traces) {
        function setYSpikes(enable) {
            for (const parameter of Object.keys(traces.layout)) {
                if (!parameter.startsWith('yaxis')) {
                    continue;
                }
                const axis = traces.layout[parameter];
                axis.showspikes = enable;
            }
        }

        function apply() {
            switch (hoverMode) {
            default:
                button.classList.remove('mdi-format-list-text', 'mdi-near-me', 'mdi-tooltip-text-outline', 'mdi-tooltip-outline');
                button.classList.add('mdi-format-list-text');
                button.title = 'Set data hover mode (current: separate labels)';
                traces.layout.hovermode = 'x';
                traces.layout.xaxis.showspikes = false
                setYSpikes(false);
                break;
            case HOVER_SINGLE_POINT:
                button.classList.remove('mdi-format-list-text', 'mdi-near-me', 'mdi-tooltip-text-outline', 'mdi-tooltip-outline');
                button.classList.add('mdi-near-me');
                button.title = 'Set data hover mode (current: single point)';
                traces.layout.hovermode = 'closest';
                traces.layout.xaxis.showspikes  = true;
                setYSpikes(true);
                break;
            case HOVER_COMBINED:
                button.classList.remove('mdi-format-list-text', 'mdi-near-me', 'mdi-tooltip-text-outline', 'mdi-tooltip-outline');
                button.classList.add('mdi-tooltip-text-outline');
                button.title = 'Set data hover mode (current: combined information)';
                traces.layout.hovermode = 'x unified';
                traces.layout.xaxis.showspikes  = true;
                setYSpikes(false);
                break;
            case HOVER_OFF:
                button.classList.remove('mdi-format-list-text', 'mdi-near-me', 'mdi-tooltip-text-outline', 'mdi-sine-wave');
                button.classList.add('mdi-tooltip-outline');
                button.title = 'Set data hover mode (current: disabled)';
                traces.layout.hovermode = false;
                traces.layout.xaxis.showspikes  = false;
                setYSpikes(false);
                break;
            }
            for (let i=0; i<options.length; i++) {
                if (hoverMode === i) {
                    options[i].button.classList.add('active');
                } else {
                    options[i].button.classList.remove('active');
                }
            }
        }

        function update(event, hover) {
            event.preventDefault();
            hoverMode = hover;
            sessionStorage.setItem('forge-plot-hover-mode', hoverMode.toString())
            apply();
            traces.updateDisplay(true);
        }

        const options = [
            {
                text: "Separate point labels",
                click: function(event) { update(event, HOVER_SEPARATE); },
            },
            {
                text: "Show nearest point only",
                click: function(event) { update(event, HOVER_SINGLE_POINT); },
            },
            {
                text: "Single label for all traces",
                click: function(event) { update(event, HOVER_COMBINED); },
            },
            {
                text: "No hover information",
                click: function(event) { update(event, HOVER_OFF); },
            },
        ];
        const button = addControlButton(options);
        button.classList.add('mdi');
        button.title = 'Set data hover mode';

        apply();
    }

    TimeSeriesCommon.installZoomHandler = function(div, realtime) {
        div.on('plotly_relayout', function(data) {
            const start_time = data['xaxis.range[0]'];
            const end_time = data['xaxis.range[1]'];
            if (!start_time || !end_time) {
                return;
            }

            const start_ms = DataSocket.fromPlotTime(start_time);
            const end_ms = DataSocket.fromPlotTime(end_time);
            if (realtime) {
                if ((end_ms - start_ms + 1001) >= TimeSelect.interval_ms) {
                    TimeSelect.zoom(undefined, undefined);
                    return;
                }
            } else {
                if (start_ms === TimeSelect.start_ms && end_ms === TimeSelect.end_ms) {
                    TimeSelect.zoom(undefined, undefined);
                    return;
                }
            }
            TimeSelect.zoom(start_ms, end_ms);
        });
        TimeSelect.applyZoom = function(start_ms, end_ms) {
            Plotly.relayout(div, {
                'xaxis.range[0]': DataSocket.toPlotTime(start_ms),
                'xaxis.range[1]': DataSocket.toPlotTime(end_ms),
            });
        }
        if (TimeSelect.isZoomed()) {
            TimeSelect.applyZoom(TimeSelect.zoom_start_ms, TimeSelect.zoom_end_ms);
        }
    }

    class DataFilter {
        constructor(traces, contaminationRecord) {
            this.epoch_ms = [];
            this.x = undefined;
            this.y = undefined;
            this.x_epoch = this.epoch_ms;

            this.contaminationSegments = [];
            if (contaminationRecord && contaminationRecord !== '') {
                DataSocket.addLoadedRecord(contaminationRecord,
                    (dataName) => {
                        return new Contamination.DataStream(dataName);
                    }, (start_ms, end_ms) => {
                        if (this.contaminationSegments.length === 0) {
                            $('button.contamination-toggle.hidden').removeClass('hidden');
                        }
                        if (this.contaminationSegments.length > 0 && this.contaminationSegments[this.contaminationSegments.length-1].end_ms === start_ms) {
                            this.contaminationSegments[this.contaminationSegments.length-1].end_ms = end_ms;
                        } else {
                            this.contaminationSegments.push({
                                start_ms: start_ms,
                                end_ms: end_ms,
                            });
                        }
                        traces.updateDisplay();
                    }, () => {
                        traces.updateDisplay(true);
                    }
                );
            }
        }

        extendData(times, values, epoch) {
            for (let i=0; i<epoch.length; i++) {
                if (this.x) {
                    this.x.push(times[i]);
                }
                if (this.y) {
                    this.y.push(values[i]);
                }
                this.epoch_ms.push(epoch[i]);
            }
        }

        clearData() {
            this.epoch_ms.length = 0;
            this.y = undefined;
            this.x = undefined;
            this.x_epoch = this.epoch_ms;
            this.contaminationSegments.length = 0;
            $('button.contamination-toggle').addClass('hidden');
        }
        
        needToApplyContamination() {
            return hideContaminatedData && this.contaminationSegments.length !== 0;
        }

        needToApplyAveraging() {
            return averagingMode !== AVERAGE_NONE;
        }

        needToApplyZoom() {
            return TimeSelect.isZoomed();
        }

        filterDataContamination(data) {
            let segmentIndex = 0;
            let i;
            for (i=0; i<this.x_epoch.length; i++) {
                const epoch_ms = this.x_epoch[i];

                for (; segmentIndex < this.contaminationSegments.length; segmentIndex++) {
                    const segment = this.contaminationSegments[segmentIndex];
                    if (epoch_ms >= segment.end_ms) {
                        continue;
                    }
                    break;
                }
                if (segmentIndex >= this.contaminationSegments.length) {
                    break;
                }

                const segment = this.contaminationSegments[segmentIndex];
                if (epoch_ms >= segment.start_ms) {
                    data.y[i] = undefined;
                }
            }
        }

        filterDataZoom(data) {
            const zoomStart = TimeSelect.zoom_start_ms;
            const zoomEnd = TimeSelect.zoom_end_ms;
            let i;
            for (i=0; i<this.x_epoch.length; i++) {
                const epoch_ms = this.x_epoch[i];
                if (epoch_ms < zoomStart) {
                    data.y[i] = undefined;
                } else {
                    break;
                }
            }

            for (let j=this.x_epoch.length-1; j > i; j--) {
                const epoch_ms = this.x_epoch[j];
                if (epoch_ms < zoomEnd) {
                    break;
                }
                data.y[j] = undefined;
            }
        }

        averageData(data) {
            if (this.epoch_ms.length === 0) {
                return;
            }

            function toAverageBegin(epoch_ms) {
                switch (averagingMode) {
                case AVERAGE_HOUR:
                    return Math.floor(epoch_ms / (60 * 60 * 1000)) * (60 * 60 * 1000);
                case AVERAGE_DAY:
                    return Math.floor(epoch_ms / (24 * 60 * 60 * 1000)) * (24 * 60 * 60 * 1000);
                case AVERAGE_MONTH:
                    let date = new Date(Math.floor(epoch_ms));
                    date.setUTCMilliseconds(0);
                    date.setUTCSeconds(0);
                    date.setUTCMinutes(0);
                    date.setUTCHours(0);
                    date.setUTCDate(1);
                    return date.getTime();
                default:
                    throw 'Unsupported averaging mode';
                }
            }
            function toAverageEnd(epoch_ms) {
                switch (averagingMode) {
                case AVERAGE_HOUR:
                    return epoch_ms + (60 * 60 * 1000);
                case AVERAGE_DAY:
                    return epoch_ms + (24 * 60 * 60 * 1000);
                case AVERAGE_MONTH:
                    let date = new Date(Math.floor(epoch_ms));
                    date.setUTCMilliseconds(0);
                    date.setUTCSeconds(0);
                    date.setUTCMinutes(0);
                    date.setUTCHours(0);
                    date.setUTCDate(1);
                    if (date.getUTCMonth() === 11) {
                        date.setUTCFullYear(date.getUTCFullYear() + 1);
                        date.setUTCMonth(0);
                    } else {
                        date.setUTCMonth(date.getUTCMonth() + 1);
                    }
                    return date.getTime();
                default:
                    throw 'Unsupported averaging mode';
                }
            }

            let outputX = [];
            let outputY = [];
            let outputEpoch = [];

            let averageBegin = toAverageBegin(this.epoch_ms[0]);
            let averageEnd = toAverageEnd(averageBegin);
            outputX.push(DataSocket.toPlotTime(averageBegin));

            let sumY = 0;
            let countY = 0;
            for (let i=0; i<this.epoch_ms.length; i++) {
                if (this.epoch_ms[i] < averageEnd) {
                    if (isFinite(data.y[i])) {
                        sumY += data.y[i];
                        countY++;
                    }
                    continue;
                }
                if (countY > 0) {
                    outputY.push(sumY / countY);
                } else {
                    outputY.push(undefined);
                }
                sumY = 0;
                countY = 0;

                averageBegin = averageEnd;
                averageEnd = toAverageEnd(averageBegin);
                while (averageEnd <= this.epoch_ms[i]) {
                    outputX.push(DataSocket.toPlotTime(averageBegin));
                    outputY.push(undefined);

                    averageBegin = averageEnd;
                    averageEnd = toAverageEnd(averageBegin);
                }

                outputEpoch.push(averageBegin);
                outputX.push(DataSocket.toPlotTime(averageBegin));
                if (isFinite(data.y[i])) {
                    sumY += data.y[i];
                    countY++;
                }
            }
            if (countY > 0) {
                outputY.push(sumY / countY);
            } else {
                outputY.push(undefined);
            }

            data.x = outputX;
            data.y = outputY;
            this.x_epoch = outputEpoch;
        }

        apply(data) {
            const applyContamination = this.needToApplyContamination();
            const applyAveraging = this.needToApplyAveraging();
            const applyZoom = this.needToApplyZoom();

            if (!applyContamination && !applyAveraging && !applyZoom) {
                if (this.y) {
                    data.y = this.y;
                    this.y = undefined;
                }
                if (this.x) {
                    data.x = this.x;
                    this.x = undefined;
                }
                this.x_epoch = this.epoch_ms;
                return;
            }
            if (!this.y) {
                this.y = data.y.slice();
            }

            this.x_epoch = this.epoch_ms;
            data.y.length = 0;
            for (let i=0; i<this.y.length; i++) {
                data.y.push(this.y[i]);
            }

            if (applyContamination || applyZoom) {
                if (this.x) {
                    data.x.length = 0;
                    for (let i=0; i<this.x.length; i++) {
                        data.x.push(this.x[i]);
                    }
                }
            }

            if (applyContamination) {
                this.filterDataContamination(data);
            }

            if (applyAveraging) {
                if (!this.x) {
                    this.x = data.x.slice();
                }
                this.averageData(data);
            }

             if (applyZoom) {
                this.filterDataZoom(data);
            }
        }

        discardBefore(data, cutoff) {
            let countDiscard = 0;
            for (; countDiscard<this.epoch_ms.length; countDiscard++) {
                if (this.epoch_ms[countDiscard] >= cutoff) {
                    break;
                }
            }
            if (countDiscard <= 0) {
                return;
            }

            this.epoch_ms.splice(0, countDiscard);
            if (this.x) {
                this.x.splice(0, countDiscard);
            } else {
                data.x.splice(0, countDiscard);
            }
            if (this.y) {
                this.y.splice(0, countDiscard);
            } else {
                data.y.splice(0, countDiscard);
            }
        }
    }

    TimeSeriesCommon.Traces = class {
        constructor(replot) {
            this._replot = replot;
            this._queuedDisplayUpdate = undefined;
            this._dataFilters = new Map();

            TimeSelect.onZoom('TimeSeriesTraces', () => {
                this.updateDisplay(true);
            });

            this._replot.handlers.push(() => {
                this.applyUpdate();
                return true;
            });
        }

        get div() { return this._replot.div; }
        get data() { return this._replot.data; }
        get layout() { return this._replot.layout; }
        get config() { return this._replot.config; }

        applyUpdate() {
            this._dataFilters.forEach((handler, traceIndex) => {
                handler.apply(this.data[traceIndex]);
            });
        }

        updateDisplay(immediate) {
            this._replot.replot(immediate)
        }

        applyDataFilter(traceIndex, contaminationRecord) {
            this._dataFilters.set(traceIndex, new DataFilter(this, contaminationRecord));
        }

        extendData(traceIndex, times, values, epoch) {
            const data = this.data[traceIndex];
            const filter = this._dataFilters.get(traceIndex);
            if (filter) {
                filter.extendData(times, values, epoch);
            }
            for (let i=0; i<times.length; i++) {
                data.x.push(times[i]);
                data.y.push(values[i]);
            }
            this.updateDisplay();
        }
        updateTimeBounds() {
            Plotly.relayout(this.div, {
                'xaxis.range': [DataSocket.toPlotTime(TimeSelect.start_ms), DataSocket.toPlotTime(TimeSelect.end_ms)],
                'xaxis.autorange': false,
            });
        }

        clearAllData() {
            this.data.forEach((data, traceIndex) => {
                if (data.x) {
                    data.x.length = 0;
                }
                if (data.y) {
                    data.y.length = 0;
                }
                if (data.z) {
                    data.z.length = 0;
                }

                const filter = this._dataFilters.get(traceIndex);
                if (filter) {
                    filter.clearData();
                }
            });
        }
    }

    TimeSeriesCommon.RealtimeTraces = class extends TimeSeriesCommon.Traces {
        updateTimeBounds() {
            TimeSelect.setIntervalBounds();
            super.updateTimeBounds();
        }

        extendData(traceIndex, times, values, epoch) {
            super.extendData(traceIndex, times, values, epoch);

            TimeSelect.setIntervalBounds();
            let discardCutoff = TimeSelect.start_ms;
            if (!TimeSelect.isZoomed()) {
                this.layout.xaxis.range = [DataSocket.toPlotTime(TimeSelect.start_ms),
                    DataSocket.toPlotTime(TimeSelect.end_ms)];
            } else {
                discardCutoff = Math.min(discardCutoff, TimeSelect.zoom_start_ms);
            }

            const data = this.data[traceIndex];
            const filter = this._dataFilters.get(traceIndex);
            if (filter) {
                 filter.discardBefore(data, discardCutoff);
            } else {
                let countDiscard = 0;
                for (; countDiscard<data.x.length; countDiscard++) {
                    const pointTime = DataSocket.fromPlotTime(data.x[countDiscard]);
                    if (pointTime >= discardCutoff) {
                        break;
                    }
                }

                if (countDiscard > 0) {
                    if (data.x) {
                        data.x.splice(0, countDiscard);
                    }
                    if (data.y) {
                        data.y.splice(0, countDiscard);
                    }
                    if (data.z) {
                        data.z.splice(0, countDiscard);
                    }
                }
            }

            this.updateDisplay();
        }
    }
})();