An access request has been submitted for {% if user.name|default(none) is not none and user.name %}{{ user.name }} <{{ user.email }}>{% else %}{{ user.email }}{% endif %}  requesting access to {{ station|upper }}.  If this is not a valid request, simply disregard this email.

{% if comment %}
The comment with the request was:
{{ comment }}
{% endif %}


To approve access for this user and station run:

    forge-vis-access grant --user={{ user.id }} {{ station }}

To grant more restricted access (e.g. to meteorological data only) refer to the command help.
