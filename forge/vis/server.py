import typing
import starlette.status
from secrets import token_urlsafe
from collections import OrderedDict
from starlette.applications import Starlette
from starlette.routing import Route, Mount, NoMatchFound
from starlette.requests import Request
from starlette.responses import Response, HTMLResponse, JSONResponse
from starlette.exceptions import HTTPException
from starlette.staticfiles import StaticFiles, FileResponse, RedirectResponse
from starlette.datastructures import Secret
from starlette.authentication import requires
from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.trustedhost import TrustedHostMiddleware
from forge.const import STATIONS
from . import CONFIGURATION
from .util import package_data, package_template, TEMPLATE_ENV
from forge.vis.mode.assemble import default_mode
import forge.vis.access.authentication
import forge.vis.view.server
import forge.vis.mode.server
import forge.vis.data.server
import forge.vis.editing.server
import forge.vis.eventlog.server
import forge.vis.status.server
import forge.vis.export.server
import forge.vis.acquisition.server
import forge.vis.dashboard.server


async def _favicon(request: Request) -> Response:
    return FileResponse(package_data('static/favicon.png'))


@requires('authenticated')
async def _stations_list(request: Request) -> Response:
    from forge.processing.station.lookup import station_data
    from forge.vis.mode.permissions import is_available
    from forge.vis.mode.assemble import lookup_mode

    try:
        data = await request.json()
        check_modes = data.get('modes')
    except:
        check_modes = None

    result: typing.Dict[str, str] = OrderedDict()
    for station in request.user.possible_stations:
        if check_modes:
            for mode_name in check_modes:
                mode_name = mode_name.lower()
                if not is_available(request, station, mode_name):
                    continue
                mode = lookup_mode(request, station, mode_name)
                if mode is None:
                    continue
                break
            else:
                continue
        result[station] = station_data(station, "site", "name")(station) or ""
    return JSONResponse(result)


async def _root(request: Request) -> Response:
    if not request.user.is_authenticated:
        try:
            return RedirectResponse(request.url_for('login'))
        except NoMatchFound:
            raise HTTPException(starlette.status.HTTP_403_FORBIDDEN, detail="Invalid authentication")

    default_station = request.query_params.get('station')
    visible_stations = request.user.visible_stations

    if len(visible_stations) == 0:
        if not request.user.can_request_access:
            try:
                return RedirectResponse(request.url_for('login'))
            except NoMatchFound:
                raise HTTPException(starlette.status.HTTP_403_FORBIDDEN, detail="Invalid authentication")
        if default_station is not None:
            default_station = default_station.lower()
            if default_station in STATIONS:
                return RedirectResponse(request.url_for('request_access') + f"?station={default_station}")
        return RedirectResponse(request.url_for('request_access'))

    if default_station is not None:
        default_station = default_station.lower()
        if default_station not in visible_stations:
            default_station = None
    if not default_station:
        for station in visible_stations:
            if default_mode(request, station) is not None:
                default_station = station
                break
    if not default_station:
        default_station = visible_stations[0]
    return HTMLResponse(await package_template('index.html').render_async(
        request=request,
        visible_stations=visible_stations,
        default_station=default_station,
    ))


TEMPLATE_ENV.globals["ENABLE_LOGIN"] = forge.vis.access.authentication.enable_login


routes = [
    Mount('/static', app=StaticFiles(directory=package_data('static')), name='static'),
    Route('/favicon.png', endpoint=_favicon),
    Route('/favicon.ico', endpoint=_favicon),

    Mount('/auth', routes=forge.vis.access.authentication.routes),
    Mount('/view', routes=forge.vis.view.server.routes),
    Mount('/editing', routes=forge.vis.editing.server.routes),
    Mount('/eventlog', routes=forge.vis.eventlog.server.routes),
    Mount('/status', routes=forge.vis.status.server.routes),
    Mount('/export', routes=forge.vis.export.server.routes),
    Mount('/station', routes=forge.vis.mode.server.routes),
    Mount('/acquisition', routes=forge.vis.acquisition.server.routes),
    Route('/settings', endpoint=forge.vis.mode.server.local_settings, name='local_settings'),

    Mount('/socket/data', routes=forge.vis.data.server.sockets),
    Mount('/socket/export', routes=forge.vis.export.server.sockets),
    Mount('/socket/acquisition', routes=forge.vis.acquisition.server.sockets),

    Route('/list/stations.json', endpoint=_stations_list, methods=['GET', 'POST']),
    Route('/list/{station}/modes.json', endpoint=forge.vis.mode.server.query_modes, methods=['POST']),
    Route('/list/{station}/views.json', endpoint=forge.vis.mode.server.list_views),

    Route('/index.html', endpoint=_root),
    Route('/index.htm', endpoint=_root),
    Route('/', endpoint=_root, name='root'),
]

middleware: typing.List[Middleware] = list()

middleware.append(Middleware(TrustedHostMiddleware,
                             allowed_hosts=CONFIGURATION.get('AUTHENTICATION.TRUSTED_HOSTS', ["*"])))

ratelimit_url = CONFIGURATION.get('RATELIMIT.REDIS')
if ratelimit_url is not None:
    from ratelimit import RateLimitMiddleware, Rule
    from ratelimit.auths.ip import client_ip
    from ratelimit.backends.redis import RedisBackend, StrictRedis, DECREASE_SCRIPT

    class Backend(RedisBackend):
        # noinspection PyMissingConstructor
        def __init__(self, url: str):
            self._redis = StrictRedis.from_url(url)
            self.decrease_function = self._redis.register_script(DECREASE_SCRIPT)

    backend = Backend(ratelimit_url)
    config = {
        r'^/auth/password/login': [Rule(minute=10)],
        r'^/auth/password/reset_issue': [Rule(minute=2, hour=20)],
        r'^/auth/password/reset': [Rule(minute=5)],
        r'^/auth/password/create': [Rule(minute=2)],
        r'^/auth/(google|microsoft|yahoo|apple)': [Rule(minute=20)],
        r'^/auth/request': [Rule(minute=10, hour=50)],
    }
    middleware.append(Middleware(RateLimitMiddleware, backend=backend, authenticate=client_ip, config=config))

middleware.append(Middleware(SessionMiddleware, session_cookie="forge_session",
                             secret_key=Secret(CONFIGURATION.get('SESSION.SECRET', token_urlsafe(32)))))
middleware.append(Middleware(AuthenticationMiddleware, backend=forge.vis.access.authentication.AuthBackend()))


dashboard_uri = CONFIGURATION.get('DASHBOARD.DATABASE')
if dashboard_uri is not None:
    routes.append(Mount('/dashboard', routes=forge.vis.dashboard.server.routes))
    routes.append(Mount('/socket/dashboard', routes=forge.vis.dashboard.server.sockets))
    middleware.append(Middleware(forge.vis.dashboard.server.DatabaseMiddleware, database_uri=dashboard_uri))


telemetry_uri = CONFIGURATION.get('TELEMETRY.DATABASE')
if telemetry_uri is not None:
    from forge.telemetry.display import DatabaseMiddleware as TelemetryDatabase
    middleware.append(Middleware(TelemetryDatabase, database_uri=telemetry_uri))


processing_uri = CONFIGURATION.get('PROCESSING.CONTROL.DATABASE')
if processing_uri is not None:
    from forge.processing.control.display import DatabaseMiddleware as ProcessingDatabase
    middleware.append(Middleware(ProcessingDatabase, database_uri=processing_uri))


app = Starlette(routes=routes, middleware=middleware)
