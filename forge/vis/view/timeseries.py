import typing
import time
from collections import OrderedDict
from enum import Enum
from math import floor
from starlette.responses import HTMLResponse
from forge.logicaltime import months_since_epoch, start_of_epoch_month_ms
from forge.vis.util import package_template
from forge.processing.station.lookup import station_data
from . import View, Request, Response


class TimeSeries(View):
    class Axis:
        def __init__(self):
            self.title: typing.Optional[str] = None
            self.range: typing.Optional[typing.Union[int, typing.Tuple[float, float]]] = None
            self.logarithmic = False
            self.format_code: typing.Optional[str] = None
            self.ticks: typing.Optional[typing.List[float]] = None

    class Trace:
        def __init__(self, axis: "TimeSeries.Axis"):
            self.axis: TimeSeries.Axis = axis
            self.legend: str = ""
            self.color: typing.Optional[str] = None
            self.format_code: typing.Optional[str] = None
            self.data_record: typing.Optional[str] = None
            self.data_field: typing.Optional[str] = None
            self.script_incoming_data: typing.Optional[str] = None

        def hover_template(self):
            fmt = self.format_code
            if not fmt:
                fmt = self.axis.format_code
            if not fmt:
                return '%{y}'
            else:
                return '%{y:' + fmt + '}'

    class Graph:
        def __init__(self):
            self.title: typing.Optional[str] = None
            self.axes: typing.List[TimeSeries.Axis] = []
            self.traces: typing.List[TimeSeries.Trace] = []
            self.contamination: typing.Optional[str] = None

        @property
        def display_title(self) -> bool:
            return bool(self.title)

    class Processing:
        def __init__(self):
            self.components: typing.List[str] = []
            self.script = str()

    def __init__(self, realtime: bool = False):
        super().__init__()
        self.realtime = realtime
        self.title: typing.Optional[str] = None
        self.graphs: typing.List[TimeSeries.Graph] = []
        self.processing: typing.Dict[str, TimeSeries.Processing] = dict()

    @property
    def required_components(self) -> typing.List[str]:
        components = OrderedDict()
        for graph in self.graphs:
            if graph.contamination:
                components['contamination'] = True
                break
        for processing in self.processing.values():
            for name in processing.components:
                components[name] = True
        return list(components.keys())

    class RequestContext:
        def __init__(self, timeseries: "TimeSeries", request: Request):
            self.timeseries = timeseries
            self.request = request

            graph_filter = request.query_params.get('graph')
            if graph_filter:
                graph_filter = str(graph_filter)
                graph_indices: typing.Set[int] = set()
                for i in graph_filter.split(','):
                    graph_indices.add(int(i.strip()) - 1)
                self.graphs: typing.List[TimeSeries.Graph] = list()
                for i in range(len(self.timeseries.graphs)):
                    if i not in graph_indices:
                        continue
                    self.graphs.append(self.timeseries.graphs[i])
            else:
                self.graphs = self.timeseries.graphs

        def __getattr__(self, item):
            return getattr(self.timeseries, item)

        @staticmethod
        def _index_code(index, base: str) -> str:
            if index == 0:
                return base
            return base + str(index + 1)

        def axis_code(self, find_axis: "TimeSeries.Axis", base: str = 'y') -> str:
            # Since we can only do overlaying axes last, the numbering is a bit odd
            index = 0
            for graph in self.graphs:
                if len(graph.axes) == 0:
                    continue
                if graph.axes[0] == find_axis:
                    return self._index_code(index, base)
                index += 1
            for graph in self.graphs:
                for axis_index in range(1, len(graph.axes)):
                    if graph.axes[axis_index] == find_axis:
                        return self._index_code(index, base)
                    index += 1
            raise KeyError

        def graph_code(self, find_graph: "TimeSeries.Graph", base: str = 'x') -> str:
            index = 0
            for graph in self.graphs:
                if graph == find_graph:
                    return self._index_code(index, base)
                index += 1
            raise KeyError

    async def __call__(self, request: Request, **kwargs) -> Response:
        return HTMLResponse(await package_template('view', 'timeseries.html').render_async(
            request=request,
            view=self.RequestContext(self, request),
            realtime=self.realtime,
            **kwargs
        ))


class PublicTimeSeries(TimeSeries):
    class Averaging(Enum):
        NONE = "none"
        HOUR = "hour"
        DAY = "day"
        MONTH = "month"

    def __init__(self, realtime: bool = True, **kwargs):
        super().__init__(realtime, **kwargs)
        self.average: "PublicTimeSeries.Averaging" = PublicTimeSeries.Averaging.NONE
        self.nominal_pressure: typing.Optional[float] = None

    async def __call__(self, request: Request, **kwargs) -> Response:
        nominal_pressure = self.nominal_pressure
        if nominal_pressure is None:
            station = kwargs.get('station')
            if station is not None:
                nominal_pressure = station_data(station, 'climatology', 'surface_pressure')(station)

        return HTMLResponse(await package_template('view', 'publictimeseries.html').render_async(
            request=request,
            view=self.RequestContext(self, request),
            realtime=self.realtime,
            nominal_pressure=nominal_pressure,
            **kwargs
        ))

    @property
    def height(self) -> typing.Optional[int]:
        return 400 * len(self.graphs)

    @property
    def interval_ms(self) -> int:
        if self.average == PublicTimeSeries.Averaging.HOUR:
            return 30 * 24 * 60 * 60 * 1000
        elif self.average == PublicTimeSeries.Averaging.DAY:
            return 365 * 24 * 60 * 60 * 1000
        elif self.average == PublicTimeSeries.Averaging.MONTH:
            return 365 * 24 * 60 * 60 * 1000
        else:
            return 5 * 24 * 60 * 60 * 1000

    @property
    def end_ms(self) -> int:
        if self.realtime:
            most_recent_possible = floor(time.time() / 60) * 60
        else:
            most_recent_possible = floor(time.time() / (60 * 60)) * 60 * 60
        if self.average == PublicTimeSeries.Averaging.HOUR:
            return int(floor(most_recent_possible / (60 * 60)) * 60 * 60 * 1000)
        elif self.average == PublicTimeSeries.Averaging.DAY:
            return int(floor(most_recent_possible / (24 * 60 * 60)) * 24 * 60 * 60 * 1000)
        elif self.average == PublicTimeSeries.Averaging.MONTH:
            month = months_since_epoch(most_recent_possible)
            return int(start_of_epoch_month_ms(month))
        else:
            return int(floor(most_recent_possible * 1000))

    @property
    def start_ms(self):
        if self.average == PublicTimeSeries.Averaging.MONTH:
            month = months_since_epoch((self.end_ms - self.interval_ms) / 1000)
            return int(start_of_epoch_month_ms(month))
        else:
            return self.end_ms - self.interval_ms
