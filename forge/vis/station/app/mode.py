import typing
from ..default.mode import Mode, ViewList, detach, aerosol_modes


station_modes = detach(aerosol_modes)


station_modes['aerosol-raw'].remove('aerosol-raw-wind')
station_modes['aerosol-realtime'].remove('aerosol-realtime-wind')
station_modes['aerosol-editing'].remove('aerosol-editing-wind')
station_modes['aerosol-clean'].remove('aerosol-clean-wind')
station_modes['aerosol-avgh'].remove('aerosol-avgh-wind')


station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-opticalscattering2', "Humidified Optical"),
                                    'aerosol-raw-optical')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-opticalscattering2', "Humidified Optical"),
                                         'aerosol-realtime-optical')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-scattering2', "Humidified Scattering"),
                                        'aerosol-editing-backscattering')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-backscattering2', "Humidified Back Scattering"),
                                        'aerosol-editing-scattering2')
station_modes['aerosol-clean'].insert(ViewList.Entry('aerosol-clean-opticalscattering2', "Humidified Optical"),
                                      'aerosol-clean-optical')
station_modes['aerosol-avgh'].insert(ViewList.Entry('aerosol-avgh-opticalscattering2', "Humidified Optical"),
                                     'aerosol-avgh-optical')

station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerzero2', "Wet Nephelometer Zero"),
                                    'aerosol-raw-nephelometerstatus')
station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerstatus2', "Wet Nephelometer Status"),
                                    'aerosol-raw-nephelometerzero2')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerzero2', "Wet Nephelometer Zero"),
                                         'aerosol-realtime-nephelometerstatus')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerstatus2', "Wet Nephelometer Status"),
                                         'aerosol-realtime-nephelometerzero2')


station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-scattering3', "Ecotech S13 Scattering"),
                                        'aerosol-editing-scattering2')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-backscattering3', "Ecotech S13 Back Scattering"),
                                        'aerosol-editing-scattering3')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-scattering4', "Ecotech S14 Scattering"),
                                        'aerosol-editing-backscattering3')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-backscattering4', "Ecotech S14 Back Scattering"),
                                        'aerosol-editing-scattering4')

station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerzero3', "S13 Nephelometer Zero"),
                                    'aerosol-raw-nephelometerstatus2')
station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerstatus3', "S13 Nephelometer Status"),
                                    'aerosol-raw-nephelometerzero3')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerzero3', "S13 Nephelometer Zero"),
                                         'aerosol-realtime-nephelometerstatus2')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerstatus3', "S13 Nephelometer Status"),
                                         'aerosol-realtime-nephelometerzero3')

station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerzero4', "S14 Nephelometer Zero"),
                                    'aerosol-raw-nephelometerstatus3')
station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-nephelometerstatus4', "S14 Nephelometer Status"),
                                    'aerosol-raw-nephelometerzero4')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerzero4', "S14 Nephelometer Zero"),
                                         'aerosol-realtime-nephelometerstatus3')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-nephelometerstatus4', "S14 Nephelometer Status"),
                                         'aerosol-realtime-nephelometerzero4')

station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-allscattering', "Scattering Comparison"),
                                    'aerosol-raw-green')
station_modes['aerosol-clean'].insert(ViewList.Entry('aerosol-clean-allscattering', "Scattering Comparison"),
                                      'aerosol-clean-green')
station_modes['aerosol-avgh'].insert(ViewList.Entry('aerosol-avgh-allscattering', "Scattering Comparison"),
                                     'aerosol-avgh-green')


station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-humidograph', "Humidograph"),
                                    'aerosol-raw-green')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-humidograph', "Humidograph"),
                                         'aerosol-realtime-green')
station_modes['aerosol-clean'].insert(ViewList.Entry('aerosol-clean-humidograph', "Humidograph"),
                                      'aerosol-clean-green')
station_modes['aerosol-avgh'].insert(ViewList.Entry('aerosol-avgh-humidograph', "Humidograph"),
                                     'aerosol-avgh-green')


station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-smps', "SMPS"),
                                    'aerosol-raw-aethalometer')
station_modes['aerosol-editing'].insert(ViewList.Entry('aerosol-editing-smps', "SMPS"),
                                        'aerosol-editing-aethalometer')
station_modes['aerosol-clean'].insert(ViewList.Entry('aerosol-clean-smps', "SMPS"),
                                      'aerosol-clean-aethalometer')
station_modes['aerosol-avgh'].insert(ViewList.Entry('aerosol-avgh-smps', "SMPS"),
                                     'aerosol-avgh-aethalometer')


station_modes['aerosol-raw'].insert(ViewList.Entry('aerosol-raw-ccnstatus', "CCN Status"),
                                    'aerosol-raw-cpcstatus')
station_modes['aerosol-realtime'].insert(ViewList.Entry('aerosol-realtime-ccnstatus', "CCN Status"),
                                         'aerosol-realtime-cpcstatus')


def get(station: str, mode_name: str) -> typing.Optional[Mode]:
    return station_modes.get(mode_name)
