import typing
from ..cpd3 import Export, ExportList, DataExportList, DataExport, Name, export_profile_get, export_profile_lookup, detach, profile_export


station_profile_export = detach(profile_export)


station_profile_export['aerosol']['raw']['scattering'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'basic', {
        Name(station, 'raw', 'T_S11'),
        Name(station, 'raw', 'P_S11'),
        Name(station, 'raw', 'U_S11'),
        Name(station, 'raw', 'T_S12'),
        Name(station, 'raw', 'P_S12'),
        Name(station, 'raw', 'U_S12'),
        Name(station, 'raw', 'T_S13'),
        Name(station, 'raw', 'P_S13'),
        Name(station, 'raw', 'U_S13'),
        Name(station, 'raw', 'T_S14'),
        Name(station, 'raw', 'P_S14'),
        Name(station, 'raw', 'U_S14'),
        Name(station, 'raw', 'BsB_S11'),
        Name(station, 'raw', 'BsG_S11'),
        Name(station, 'raw', 'BsR_S11'),
        Name(station, 'raw', 'BbsB_S11'),
        Name(station, 'raw', 'BbsG_S11'),
        Name(station, 'raw', 'BbsR_S11'),
        Name(station, 'raw', 'BsB_S12'),
        Name(station, 'raw', 'BsG_S12'),
        Name(station, 'raw', 'BsR_S12'),
        Name(station, 'raw', 'BbsB_S12'),
        Name(station, 'raw', 'BbsG_S12'),
        Name(station, 'raw', 'BbsR_S12'),
        Name(station, 'raw', 'BsB_S13'),
        Name(station, 'raw', 'BsG_S13'),
        Name(station, 'raw', 'BsR_S13'),
        Name(station, 'raw', 'BbsB_S13'),
        Name(station, 'raw', 'BbsG_S13'),
        Name(station, 'raw', 'BbsR_S13'),
        Name(station, 'raw', 'BsB_S14'),
        Name(station, 'raw', 'BsG_S14'),
        Name(station, 'raw', 'BsR_S14'),
        Name(station, 'raw', 'BbsB_S14'),
        Name(station, 'raw', 'BbsG_S14'),
        Name(station, 'raw', 'BbsR_S14'),
        Name(station, 'raw', 'T_V12'),
        Name(station, 'raw', 'U_V12'),
    },
)
station_profile_export['aerosol']['clean']['scattering'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'basic', {
        Name(station, 'clean', 'T_S11'),
        Name(station, 'clean', 'P_S11'),
        Name(station, 'clean', 'U_S11'),
        Name(station, 'clean', 'T_S12'),
        Name(station, 'clean', 'P_S12'),
        Name(station, 'clean', 'U_S12'),
        Name(station, 'clean', 'T_S13'),
        Name(station, 'clean', 'P_S13'),
        Name(station, 'clean', 'U_S13'),
        Name(station, 'clean', 'T_S14'),
        Name(station, 'clean', 'P_S14'),
        Name(station, 'clean', 'U_S14'),
        Name(station, 'clean', 'BsB_S11'),
        Name(station, 'clean', 'BsG_S11'),
        Name(station, 'clean', 'BsR_S11'),
        Name(station, 'clean', 'BbsB_S11'),
        Name(station, 'clean', 'BbsG_S11'),
        Name(station, 'clean', 'BbsR_S11'),
        Name(station, 'clean', 'BsB_S12'),
        Name(station, 'clean', 'BsG_S12'),
        Name(station, 'clean', 'BsR_S12'),
        Name(station, 'clean', 'BbsB_S12'),
        Name(station, 'clean', 'BbsG_S12'),
        Name(station, 'clean', 'BbsR_S12'),
        Name(station, 'clean', 'BsB_S13'),
        Name(station, 'clean', 'BsG_S13'),
        Name(station, 'clean', 'BsR_S13'),
        Name(station, 'clean', 'BbsB_S13'),
        Name(station, 'clean', 'BbsG_S13'),
        Name(station, 'clean', 'BbsR_S13'),
        Name(station, 'clean', 'BsB_S14'),
        Name(station, 'clean', 'BsG_S14'),
        Name(station, 'clean', 'BsR_S14'),
        Name(station, 'clean', 'BbsB_S14'),
        Name(station, 'clean', 'BbsG_S14'),
        Name(station, 'clean', 'BbsR_S14'),
        Name(station, 'clean', 'T_V12'),
        Name(station, 'clean', 'U_V12'),
    },
)
station_profile_export['aerosol']['avgh']['scattering'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'basic', {
        Name(station, 'avgh', 'T_S11'),
        Name(station, 'avgh', 'P_S11'),
        Name(station, 'avgh', 'U_S11'),
        Name(station, 'avgh', 'T_S12'),
        Name(station, 'avgh', 'P_S12'),
        Name(station, 'avgh', 'U_S12'),
        Name(station, 'avgh', 'T_S13'),
        Name(station, 'avgh', 'P_S13'),
        Name(station, 'avgh', 'U_S13'),
        Name(station, 'avgh', 'T_S14'),
        Name(station, 'avgh', 'P_S14'),
        Name(station, 'avgh', 'U_S14'),
        Name(station, 'avgh', 'BsB_S11'),
        Name(station, 'avgh', 'BsG_S11'),
        Name(station, 'avgh', 'BsR_S11'),
        Name(station, 'avgh', 'BbsB_S11'),
        Name(station, 'avgh', 'BbsG_S11'),
        Name(station, 'avgh', 'BbsR_S11'),
        Name(station, 'avgh', 'BsB_S12'),
        Name(station, 'avgh', 'BsG_S12'),
        Name(station, 'avgh', 'BsR_S12'),
        Name(station, 'avgh', 'BbsB_S12'),
        Name(station, 'avgh', 'BbsG_S12'),
        Name(station, 'avgh', 'BbsR_S12'),
        Name(station, 'avgh', 'BsB_S13'),
        Name(station, 'avgh', 'BsG_S13'),
        Name(station, 'avgh', 'BsR_S13'),
        Name(station, 'avgh', 'BbsB_S13'),
        Name(station, 'avgh', 'BbsG_S13'),
        Name(station, 'avgh', 'BbsR_S13'),
        Name(station, 'avgh', 'BsB_S14'),
        Name(station, 'avgh', 'BsG_S14'),
        Name(station, 'avgh', 'BsR_S14'),
        Name(station, 'avgh', 'BbsB_S14'),
        Name(station, 'avgh', 'BbsG_S14'),
        Name(station, 'avgh', 'BbsR_S14'),
        Name(station, 'avgh', 'T_V12'),
        Name(station, 'avgh', 'U_V12'),
    },
)


station_profile_export['aerosol']['raw']['aethalometer'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'unsplit', set(
        [Name(station, 'raw', f'F1_A81')] +
        [Name(station, 'raw', f'Ba{i + 1}_A81') for i in range(7)] +
        [Name(station, 'raw', f'X{i + 1}_A81') for i in range(7)] +
        [Name(station, 'raw', f'ZFACTOR{i + 1}_A81') for i in range(7)] +
        [Name(station, 'raw', f'Ir{i + 1}_A81') for i in range(7)]
    )
)
station_profile_export['aerosol']['clean']['aethalometer'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'unsplit', set(
        [Name(station, 'clean', f'F1_A81')] +
        [Name(station, 'clean', f'Ba{i + 1}_A81') for i in range(7)] +
        [Name(station, 'clean', f'X{i + 1}_A81') for i in range(7)] +
        [Name(station, 'clean', f'ZFACTOR{i + 1}_A81') for i in range(7)] +
        [Name(station, 'clean', f'Ir{i + 1}_A81') for i in range(7)]
    )
)
station_profile_export['aerosol']['avgh']['aethalometer'].data = lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
    start_epoch_ms, end_epoch_ms, directory, 'average', set(
        [Name(station, 'avgh', f'F1_A81')] +
        [Name(station, 'avgh', f'Ba{i + 1}_A81') for i in range(7)] +
        [Name(station, 'avgh', f'X{i + 1}_A81') for i in range(7)] +
        [Name(station, 'avgh', f'ZFACTOR{i + 1}_A81') for i in range(7)] +
        [Name(station, 'avgh', f'Ir{i + 1}_A81') for i in range(7)]
    )
)


station_profile_export['aerosol']['raw'].insert(
    DataExportList.Entry('ccn', "CCN", lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
        start_epoch_ms, end_epoch_ms, directory, 'unsplit', {
            Name(station, 'raw', 'N_N12'),
            Name(station, 'raw', 'Nb_N12'),
            Name(station, 'raw', 'Tu_N12'),
            Name(station, 'raw', 'T1_N12'),
            Name(station, 'raw', 'T2_N12'),
            Name(station, 'raw', 'T3_N12'),
            Name(station, 'raw', 'T4_N12'),
            Name(station, 'raw', 'T5_N12'),
            Name(station, 'raw', 'T6_N12'),
            Name(station, 'raw', 'Q1_N12'),
            Name(station, 'raw', 'Q2_N12'),
            Name(station, 'raw', 'U_N12'),
            Name(station, 'raw', 'P_N12'),
            Name(station, 'raw', 'DT_N12'),
        },
    )),
)
station_profile_export['aerosol']['clean'].insert(
    DataExportList.Entry('ccn', "CCN", lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
        start_epoch_ms, end_epoch_ms, directory, 'unsplit', {
            Name(station, 'clean', 'N_N12'),
            Name(station, 'clean', 'Nb_N12'),
            Name(station, 'clean', 'Tu_N12'),
            Name(station, 'clean', 'T1_N12'),
            Name(station, 'clean', 'T2_N12'),
            Name(station, 'clean', 'T3_N12'),
            Name(station, 'clean', 'T4_N12'),
            Name(station, 'clean', 'T5_N12'),
            Name(station, 'clean', 'T6_N12'),
            Name(station, 'clean', 'Q1_N12'),
            Name(station, 'clean', 'Q2_N12'),
            Name(station, 'clean', 'U_N12'),
            Name(station, 'clean', 'P_N12'),
            Name(station, 'clean', 'DT_N12'),
        },
    )),
)
station_profile_export['aerosol']['avgh'].insert(
    DataExportList.Entry('ccn', "CCN", lambda station, start_epoch_ms, end_epoch_ms, directory: DataExport(
        start_epoch_ms, end_epoch_ms, directory, 'average', {
            Name(station, 'avgh', 'N_N12'),
            Name(station, 'avgh', 'Nb_N12'),
            Name(station, 'avgh', 'Tu_N12'),
            Name(station, 'avgh', 'T1_N12'),
            Name(station, 'avgh', 'T2_N12'),
            Name(station, 'avgh', 'T3_N12'),
            Name(station, 'avgh', 'T4_N12'),
            Name(station, 'avgh', 'T5_N12'),
            Name(station, 'avgh', 'T6_N12'),
            Name(station, 'avgh', 'Q1_N12'),
            Name(station, 'avgh', 'Q2_N12'),
            Name(station, 'avgh', 'U_N12'),
            Name(station, 'avgh', 'P_N12'),
            Name(station, 'avgh', 'DT_N12'),
        },
    )),
)


def get(station: str, mode_name: str, export_key: str,
        start_epoch_ms: int, end_epoch_ms: int, directory: str) -> typing.Optional[Export]:
    return export_profile_get(station, mode_name, export_key,
                              start_epoch_ms, end_epoch_ms, directory, station_profile_export)


async def visible(station: str, mode_name: str) -> typing.Optional[ExportList]:
    return export_profile_lookup(station, mode_name, station_profile_export)
