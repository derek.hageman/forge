import typing
from ..default.dashboard import code_records as base_code_records, record as base_record, Record, FileIngestRecord


code_records = dict(base_code_records)

code_records['aerosol-raw-ingest'] = FileIngestRecord.simple_override(
    name="Ingest uploaded FTP data",
)


def record(station: typing.Optional[str], code: str) -> typing.Optional[Record]:
    return code_records.get(code) or base_record(station, code)
