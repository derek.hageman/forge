from enum import IntEnum


class ConnectionType(IntEnum):
    WRITE = 0
    STREAM = 1
    READ = 2
